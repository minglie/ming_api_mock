
//是否生成服务
M.enable_server=true;


console.log("ReadMe: https://www.bilibili.com/video/BV1F5411n7z7")

app.begin((req, res) => {
    console.log("req ", req.url)
    var pathname = url_module.parse(req.url).pathname;
}) 

/**
 * 服务注册
 */
app.post("registServer",(req, res) => {
    //console.log(req.body)
    let reqObj= JSON.parse(req.body)
    M.map_path=reqObj.map_path;
    M.setAttribute(reqObj.path,reqObj.res)
    if(M.enable_server){
        if("./getServer.json"==M.map_path){
          app.get(reqObj.path,(req,res)=>{res.send(JSON.stringify(reqObj.res))})
        }else if("./postServer.json"==M.map_path){
          app.post(reqObj.path,(req,res)=>{res.send(JSON.stringify(reqObj.res))})
        }else{
          app.mapping(reqObj.path,(req,res)=>{res.send(JSON.stringify(reqObj.res))})
        }
    }
    res.send("ok")
}) 


/**
 * 服务加载
 */
app.get("loadServer",async (req, res) => {
    let getJson= M.getObjByFile("getServer.json")
    let postJson= M.getObjByFile("postServer.json")
    let mappingJson= M.getObjByFile("mappingServer.json")
    let getJsonKeys=  Object.keys(getJson)
    let postJsonKeys=  Object.keys(postJson)
    let mappingJsonKeys=  Object.keys(mappingJson)
    console.log("getJsonKeys",getJsonKeys)
    console.log("postJsonKeys",postJsonKeys)
    for (let i=0;i<getJsonKeys.length;i++) {
        app.get(getJsonKeys[i],(req,res)=>{res.send( JSON.stringify(getJson[getJsonKeys[i]]))})
    }
    for (let i=0;i<postJsonKeys.length;i++) {
        app.post(postJsonKeys[i],(req,res)=>{res.send( JSON.stringify(postJson[postJsonKeys[i]]))})
    }
    for (let i=0;i<mappingJsonKeys.length;i++) {
        app.post(mappingJsonKeys[i],(req,res)=>{res.send( JSON.stringify(mappingJson[mappingJsonKeys[i]]))})
    }
    res.send("ok");
  }
) 


M.__default_file["__default_server.js"]=`let getJson= M.getObjByFile("getServer.json")
let postJson= M.getObjByFile("postServer.json")
let getJsonKeys=  Object.keys(getJson)
let postJsonKeys=  Object.keys(postJson)
console.log("getJsonKeys",getJsonKeys)
console.log("postJsonKeys",postJsonKeys)
for (let i=0;i<getJsonKeys.length;i++) {
    app.get(getJsonKeys[i],(req,res)=>{res.send( JSON.stringify(getJson[getJsonKeys[i]]))})
}
for (let i=0;i<postJsonKeys.length;i++) {
    app.post(postJsonKeys[i],(req,res)=>{res.send( JSON.stringify(postJson[postJsonKeys[i]]))})
}`;


