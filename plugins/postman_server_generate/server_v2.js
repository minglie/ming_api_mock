
//是否生成服务
M.enable_server=true;

console.log("ReadMe: https://www.bilibili.com/video/BV1F5411n7z7")

/**
 * 第一次启动服务时加载一次服务文件
 */

let mappingJson= M.getObjByFile("mappingServer.json")
let mappingJsonKeys=  Object.keys(mappingJson)
console.log("mappingJsonKeys",mappingJsonKeys)
for (let i=0;i<mappingJsonKeys.length;i++) {
    app.mapping(mappingJsonKeys[i],(req,res)=>{res.send(mappingJson[mappingJsonKeys[i]])})
}
console.log("成功加载"+mappingJsonKeys.length+"个接口")



app.begin((req, res) => {
    console.log("req ", req.url, req.params)
    var pathname = url_module.parse(req.url).pathname;
}) 

/**
 * 服务注册
 */
app.post("registServer",(req, res) => {
    //console.log(req.body)
    let reqObj= JSON.parse(req.body)
    M.map_path=reqObj.map_path;
    M.setAttribute(reqObj.path,reqObj.res)
    if(M.enable_server){
        if("./getServer.json"==M.map_path){
          app.get(reqObj.path,(req,res)=>{res.send(JSON.stringify(reqObj.res))})
        }else if("./postServer.json"==M.map_path){
          app.post(reqObj.path,(req,res)=>{res.send(JSON.stringify(reqObj.res))})
        }else{
          app.mapping(reqObj.path,(req,res)=>{res.send(JSON.stringify(reqObj.res))})
        }
    }
    res.send("ok")
}) 



/**
 * postman 注册服务用
 */
M.__default_file["__default_index.html"]=` 
        map_path="./mappingServer.json";
        const regRequest = {
                    url: 'http://localhost:9999/registServer',
                    method: 'POST',
                    header:  ['Content-Type: application/json'],  
                    body: {
                        mode: 'raw',  
                        raw: JSON.stringify({ path: pm.request.url.path.join("/"), "map_path": map_path ,res: pm.response.json()})


                    }
        };
        //发送请求
        pm.sendRequest(regRequest, function (err, res) {
                console.log("<===",err ? err : res);  
        });
`;
