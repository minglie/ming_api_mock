
#### 目的

这个插件的目的是将postman或者浏览器调过的接口直接转存为后端服务或ming_mock服务
可先借助Proxy SwitchyOmega 与 postman的Capture requests拦截器生成
postman测试用例,再用postman的testCase脚本与本插件生成后端的mock服务



# 插件安装
``` bash
mock https://minglie.gitee.io/ming_api_mock/plugins/postman_server_generate/server.js 9999
```

# 服务注册
``` bash
    curl --location --request POST 'http://localhost:9999/registServer' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "path":"/a",
        "map_path":"./getServer.json",
        "res":"{}"
    }'
```

# 对应postman的testCase脚本
https://gitee.com/minglie/ming_api_mock/raw/master/plugins/postman_server_generate/postmanTestCase.js

# 对应油猴插件脚本
https://gitee.com/minglie/ming_api_mock/raw/master/plugins/postman_server_generate/ajax_interceptor.js


# 也可以将生成的服务转存位ming_mock服务
https://gitee.com/minglie/ming_api_mock/raw/master/plugins/postman_server_generate/static/index.js

