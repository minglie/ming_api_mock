// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
 // @match        *://*/*
// @grant        none
// ==/UserScript==

(function () {

  document.title = new Date().getTime();

function registMockServer(config){
if(config.path.startsWith("api")){
 let  map_path="./getServer.json";
  if("GET"==config.requestMethed){
    map_path="./getServer.json";
  } else if("POST"==config.requestMethed){
    map_path="./postServer.json";
  }else{
      map_path="./mappingServer.json";
  }

  let res=config.responseBody;
  try{
    res=JSON.parse(config.responseBody)
  }catch(e){

  }
  var raw = JSON.stringify({"path":config.path,"map_path":map_path,"res": res});
  var requestOptions = {
    method: 'POST',
    body: raw
  };
  fetch("http://localhost:9999/registServer", requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));
      console.log(config)
    }
}



let ajax_interceptor_qoweifjqon = {
  settings: {
  },
  originalXHR: window.XMLHttpRequest,
  myXHR: function() {
    const modifyResponse =async (d) => {
         let config={}
         config.path=this.responseURL.replace(/((http)|(https)):\/\/\S+?\/|(\?.*)/g, "")
         config.requestMethed=this.requestMethed;
         config.responseBody=this.responseText
         registMockServer(config);
    }
    const xhr = new ajax_interceptor_qoweifjqon.originalXHR;
    for (let attr in xhr) {
      if (attr === 'onreadystatechange') {
        xhr.onreadystatechange =async (...args) => {
          if (this.readyState == 4) {
             await  modifyResponse(args);
          }
          this.onreadystatechange && this.onreadystatechange.apply(this, args);
        }
        continue;
      } else if (attr === 'onload') {
        xhr.onload =async (...args) => {
          await modifyResponse(args);
          this.onload && this.onload.apply(this, args);
        }
        continue;
      }
      if (typeof xhr[attr] === 'function') {
        if(attr=="open"){
          this[attr] =(...args)=>{
            //console.log("aa",args)
            this.requestMethed=args[0];
            return xhr[attr].bind(xhr)(...args);
           }
        }else{
          this[attr] = xhr[attr].bind(xhr);
        }
      } else {
        // responseText和response不是writeable的，但拦截时需要修改它，所以修改就存储在this[`_${attr}`]上
        if (attr === 'responseText' || attr === 'response') {
          Object.defineProperty(this, attr, {
            get: () => this[`_${attr}`] == undefined ? xhr[attr] : this[`_${attr}`],
            set: (val) => this[`_${attr}`] = val,
            enumerable: true
          });
        } else {
          Object.defineProperty(this, attr, {
            get: () => xhr[attr],
            set: (val) => xhr[attr] = val,
            enumerable: true
          });
        }
      }
    }
  }
}
if(true){
  window.XMLHttpRequest = ajax_interceptor_qoweifjqon.myXHR;
}

})();