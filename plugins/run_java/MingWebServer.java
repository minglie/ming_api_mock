import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



class HttpUtils{
	/**
	 * Created by chengxia on 2018/12/4.
	 */
	public static String post(String URL,String jsonStr){
		OutputStreamWriter out = null;
		BufferedReader in = null;
		StringBuilder result = new StringBuilder();
		HttpURLConnection conn = null;
		try{
			URL url = new URL(URL);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			//发送POST请求必须设置为true
			conn.setDoOutput(true);
			conn.setDoInput(true);
			//设置连接超时时间和读取超时时间
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(10000);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			//获取输出流
			out = new OutputStreamWriter(conn.getOutputStream());
			//向post请求发送json数据
			out.write(jsonStr);
			out.flush();
			out.close();
			//取得输入流，并使用Reader读取
			if (200 == conn.getResponseCode()){
				in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
				String line;
				while ((line = in.readLine()) != null){
					result.append(line);
					System.out.println(line);
				}
			}else{
				System.out.println("ResponseCode is an error code:" + conn.getResponseCode());
			}
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			try{
				if(out != null){
					out.close();
				}
				if(in != null){
					in.close();
				}
			}catch (IOException ioe){
				ioe.printStackTrace();
			}
		}
		return result.toString();
	}

	public static String get(String URL){
		HttpURLConnection conn = null;
		InputStream is = null;
		BufferedReader br = null;
		StringBuilder result = new StringBuilder();
		try{
			//创建远程url连接对象
			URL url = new URL(URL);
			//通过远程url连接对象打开一个连接，强转成HTTPURLConnection类
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			//设置连接超时时间和读取超时时间
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(60000);
			conn.setRequestProperty("Accept", "application/json");
			//发送请求
			conn.connect();
			//通过conn取得输入流，并使用Reader读取
			if (200 == conn.getResponseCode()){
				is = conn.getInputStream();
				br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				String line;
				while ((line = br.readLine()) != null){
					result.append(line);
				}
			}else{
				System.out.println("ResponseCode is an error code:" + conn.getResponseCode());
			}
		}catch (MalformedURLException e){
			e.printStackTrace();
		}catch (IOException e){
			e.printStackTrace();
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			try{
				if(br != null){
					br.close();
				}
				if(is != null){
					is.close();
				}
			}catch (IOException ioe){
				ioe.printStackTrace();
			}
			conn.disconnect();
		}
		return result.toString();
	}

	public static String readFile(File file) {
		InputStreamReader read = null;
		try {
			read = new InputStreamReader(new FileInputStream(file), "utf-8");
			BufferedReader bufferedReader = new BufferedReader(read);
			String lineTxt = null;
			StringBuffer buffer = new StringBuffer();
			String line = " ";
			while ((line = bufferedReader.readLine()) != null) {
				buffer.append(line);
			}
			read.close();
			return buffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
}




public class MingWebServer {

	public static String g_note="名列";
    public static Map<String, String> reqMap = new HashMap();

	public static Map getRequest(Object obj) {
		if (obj != null) {
			if (obj.toString().length() > 10) {
				//path
				String path[];
				String getPath[] = obj.toString().split("\n")[0].substring(4).split(" ")[0].split("\\?");
			    reqMap.put("requestMethed",obj.toString().substring(0,obj.toString().indexOf(" ")));
			    if ("POST".equals(reqMap.get("requestMethed"))){
					String postPayh[] =obj.toString().split("HTTP")[0].split("POST")[1].trim().split("\\?");
					path=postPayh;
				} else {
					path=getPath;
				}
				reqMap.put("path", path[0]);
				//params
				if (path.length > 1) {
					reqMap.put("query", path[1]);
				} else {
					reqMap.put("query", "");
				}
				String[] split = obj.toString().split("\n\r");
				if(split.length>=2){
					reqMap.put("body", split[1].trim());
				}
			}
		}
		return reqMap;
	}

	public static String getResponseJson(String json) {
		String out = "HTTP/1.1 200 OK\n";
		out += "Server: Apache-Coyote/1.1\n";
		out += "Content-Type: application/json; charset=utf-8\n";
		out += "Content-Length: " + json.getBytes().length + "\n";
		out += "Date: Fri, 11 May 2012 07:51:39 GMT\n";
		out += "Connection: close\n";
		out += "\n";
		out += json;
		return out;
	}

	public static String getResponseHtml(String html) {
		String out = "HTTP/1.1 200 OK\n";
		out += "Server: Apache-Coyote/1.1\n";
		out += "Content-Type: text/html; charset=utf-8\n";
		out += "Date: Fri, 11 May 2012 07:51:39 GMT\n";
		out += "\n"+html;
		return out;
	}

	public static String getParameter(String name) {
		Pattern p = Pattern.compile(name + "=([^&]*)(&|$)");
		Matcher m = p.matcher(reqMap.get("query"));
		while (m.find()) {
			return m.group(1);
		}
		return "null";
	}


	public static void server(final Function<Map, String> handler, int port) throws Exception {
		ServerSocket ss = new ServerSocket();
		ss.bind(new InetSocketAddress(port));
		System.out.println("Listener on:" + port);
		while (true) {
			final Socket s = ss.accept();
			InputStream inputStream = s.getInputStream();
			byte[] buf = new byte[102400];
			int len = inputStream.read(buf);
			String text = new String(buf, 0, len + 1);
			if (text == null || text.length() == 0) {
				s.shutdownOutput();
				continue;
			}
			Map request = getRequest(text);
			request.put("socket",s);
			request.put("alreadySend",false);
			request.put("sent", new Consumer<String>() {
				@Override
				public void accept(String  o) {
					try {
						send(request,o);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			String returnStr = (String) handler.apply(request);
			if("/".equals(request.get("path"))){
					indexHtml(request);
			}else if("/a.html".equals(request.get("path"))){
					renderHtml(request,g_note);
			} else {
				if(returnStr !=null){
					send(request,returnStr);
				}else {
					send(request,"ok");
				}
			}
		}
	}

	public static void indexHtml(Map request) throws IOException {
		renderHtml(request,"\n" +
				"<!DOCTYPE html>\n" +
				"<html lang=\"en\">\n" +
				"<head>\n" +
				"    <meta charset=\"UTF-8\">\n" +
				"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
				"    <title>Document</title>\n" +
				"    <style type=\"text/css\">\n" +
				"            #text_input {\n" +
				"                width: 100%;\n" +
				"                height: 1000px;\n" +
				"                line-height: 1;\n" +
				"                background: rgba(100, 200, 50, 0.3);\n" +
				"                \n" +
				"            }\n" +
				"            #btnId{\n" +
				"                margin: 10px;\n" +
				"                width: 100%;\n" +
				"                height: 50px;\n" +
				"            }\n" +
				"    </style>\n" +
				"</head>\n" +
				"<body>\n" +
				"    \n" +
				"    <button id=\"btnId\">run</button>\n" +
				"    <textarea id=\"text_input\" rows=\"10\" cols=\"50\"></textarea>\n" +
				"    <script>\n" +
				"    fetch(\"/a.html\")\n" +
				"     .then((res)=>res.text())\n" +
				"     .then(data=>{\n" +
				"        text_input.value=data;\n" +
				"     });\n" +
				"       btnId.onclick=function(){\n" +
				"           fetch('/p', {\n" +
				"            method: 'POST',\n" +
				"            mode: 'cors',\n" +
				"            credentials: 'include',\n" +
				"            headers: {\n" +
				"                'Content-Type': 'text/plain'\n" +
				"            },\n" +
				"            body: text_input.value\n" +
				"            }).then(function(response) {\n" +
				"                btnId.innerText=response.status+new Date().getTime()\n" +
				"            }).catch(e=>{ \n" +
				"                btnId.innerText=e\n" +
				"            })\n" +
				"       }\n" +
				"\n" +
				"\n" +
				"    </script>\n" +
				"</body>\n" +
				"</html>");
	}

	public static void send(Map request,String res)  {
		send(request,"json",res);
	}

	public static void renderHtml(Map request,String res)  {
		send(request,"html",res);
	}

	public static void renderUrl(Map request,String url){
		String text = getRemoteCacheByUrl(url);
		send(request,"html",text);
	}

	public static void send(Map request,String contentType,String res)  {
		try {
			if((Boolean) request.get("alreadySend")){
				return;
			}
			Socket s = (Socket)request.get("socket");
			PrintWriter pw = new PrintWriter(s.getOutputStream());
			if("html".equals(contentType)){
				pw.println(getResponseHtml(res));
			}else {
				pw.println(getResponseJson(res));
			}
			pw.flush();
			s.shutdownOutput();
			request.put("alreadySend",true);
		}catch (Exception e){
			e.printStackTrace();
		}

	}

	public static Map<String,Function<Map  ,String>> g_functionMap=new HashMap<>();
	public static Map<String,String> globle_cacheMap=new HashMap<>();
	public static Consumer<Map> g_begin=d->{
		System.out.println(d);
	};

	public static String getRemoteCacheByUrl(String url) {
		if(globle_cacheMap.containsKey(url)){
			return globle_cacheMap.get(url);
		}
		String text="";
		System.out.println("req remote url:"+url);
		if(url.startsWith("file:")){
			text=HttpUtils.readFile(new File(url.substring(5)));
		}else {
			text=HttpUtils.get(url);
		}
		globle_cacheMap.put(url,text);
		return text;
	}

	public static void begin(Consumer<Map> beginFunction){
		g_begin=beginFunction;
	}

	public static void main(String[] args) throws Exception {
		Integer port=8889;
		if(args.length!=0){
			port=Integer.valueOf(args[0]) ;
		}
		server((req) -> {
			//begin
			g_begin.accept(req);
			if( !"".equals((String) req.get("body"))){
				g_note=(String) req.get("body");
			}
			if(g_functionMap.containsKey(req.get("path"))){
				return g_functionMap.get(req.get("path")).apply(req);
			}
			return null;
		}, port);
	}



	static {
		begin(requet-> {
			System.out.println("requestMethed:"+requet.get("requestMethed"));
			System.out.println("path:"+requet.get("path"));
			System.out.println("query:"+requet.get("query"));
			System.out.println("body:"+requet.get("body"));
			System.out.println("alreadySend:"+requet.get("alreadySend"));
		});

		g_functionMap.put("/postTest",(requet)->{
			renderUrl(requet,"https://www.baidu.com/");

			renderHtml(requet,"https://www.baidu.com/");
			//((Consumer<String>)req.get("sent")).accept("EEEEEEEEEEEEEEEEEEE");
			return "wwwwwww";
		});

	}



}