
//远程springBoot服务地址
M.remoteServerHost='http://localhost:8080';

M.writeFile("T1.java","")

let s={}
s["/index.html"]=async (req,res)=>{
    await  res.renderUrl("https://minglie.gitee.io/ming_api_mock/plugins/run_java/index.html")
}

//把所有请求ming_node的路径都打印出来
app.begin(async (req, res) => {
    console.log("req ", req.url)
    var pathname = url_module.parse(req.url).pathname;
    if (Object.keys(s).indexOf(pathname)>-1) {
       await  s[pathname](req,res)
    }
})





//文件保存后的hook函数
M.endRun=async (file)=>{
    if(file.file.endsWith("java")){
        M.postJson(M.remoteServerHost+"/minglie/runjava/runcode",{
              code:file.fun
            },
            { 'Content-Type': 'application/json;charset=UTF-8'})
            .then(d=>{
                console.log(d)
            })
    }
};

//sse页面查远程服务地址用
app.get("/remoteHost",(req,res)=>{
    res.send(M.remoteServerHost)
})






