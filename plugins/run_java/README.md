# 目的

 这个插件的目的是将远程spring boot服务的java环境作为我们java代码的运行环境,这样我们就可以使用到远程服务的网络,
以及整个spring容器中的所有bean,解决了需调试代码但本地无运行环境的问题,减少为解决bug而重复部署的痛苦

此插件只是提高使用体验,并不是必须的,直接用postman也行,只需将RunJavaController.java 拷贝到项目中即可,

下面演示的例子假设远程服务地址为:
http://localhost:8080


# 插件安装
``` bash
mock https://minglie.gitee.io/ming_api_mock/plugins/run_java/test_case/server.js
```

# springBoot端代码
https://gitee.com/minglie/ming_api_mock/blob/master/plugins/run_java/RunJavaController.java


# 使用此插件调用执行java代码地址
 访问 

 http://localhost:8888/

 编辑T1.java 并Run


# 使用此插件展示控制台日志

 访问 

 http://localhost:8888/index.html


# 测试java代码例1
获取容器的bean,获取容器的配置,因为代码编译是再springBoot进程中进行的,所以下面代码是合法的 
``` java
package com.mingruncode.utils;
import com.mingruncode.App;
import com.mingruncode.service.Userservice;
import org.springframework.core.env.Environment;
public class T01 {
    public static void main(String[] args) {
        Userservice userservice =(Userservice)App.applicationContext.getBean("userservice");
        System.out.println(userservice.getVal());
        Environment environment = App.applicationContext.getEnvironment();
        String property = environment.getProperty("server.port");
        System.out.println(property);
    }
}
```

# 已经定位到了bug的范围,在bug处注入RunJavaController.hook
远程bug除的代码注入RunJavaController.hook
``` java
package com.mingruncode.service;
import com.mingruncode.controller.RunJavaController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class Userservice {
    
    @Value("444")
    private String po;

    /**
     *  可能出现bug的地方注入 RunJavaController.hook
     * @return
     */
    public String getVal(){
        Object bugOutParam = RunJavaController.hook.apply("bugInputParam");
        RunJavaController.sendSSE("bugOutParam:"+bugOutParam);
        return po;
    }
}

```

# 本地代码自定义RunJavaController.hook
``` java
package com.mingruncode.utils;
import com.mingruncode.App;
import com.mingruncode.controller.RunJavaController;
import com.mingruncode.service.Userservice;
import org.springframework.core.env.Environment;
import java.util.function.Function;

public class T011 {
    public static void main(String[] args) {
        RunJavaController.hook=new Function<Object, Object>() {
            @Override
            public Object apply(Object o) {
                System.out.println("VVVVVVVVVVVVVVVVV");
                System.err.println(o.toString());
                return o;
            }
        };
        Userservice userservice =(Userservice)App.applicationContext.getBean("userservice");
        System.out.println(userservice.getVal());
  
    }
}

```

# 找不到日志使用  RunJavaController.sendSSE 打到本地

``` java
   RunJavaController.sendSSE("hello word");
```


# 不使用此插件调用执行java代码
``` bash
curl --location --request POST 'http://localhost:8080/minglie/runjava/runcode' \
--header 'accept: application/json, text/javascript, */*; q=0.01' \
--header 'accept-encoding: gzip, deflate, br' \
--header 'accept-language: zh-CN,zh;q=0.9' \
--header 'content-type: application/json;charset=utf-8' \
--header 'host: localhost:8888' \
--header 'proxy-connection: keep-alive' \
--header 'referer: http://localhost:8888/index.html' \
--header 'sec-fetch-dest: empty' \
--header 'sec-fetch-mode: cors' \
--header 'sec-fetch-site: same-origin' \
--header 'user-agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36' \
--header 'x-requested-with: XMLHttpRequest' \
--data-raw '{
    "code":"package com.mingruncode.utils; public class T {public static void main(String[] args) {System.out.println(\"BBB\");}\n}\n"
}'
```

# 不使用此插件查看代码控制台日志地址
 http://localhost:8080/minglie/runjava/sse



## 
## 
## 

#  如果不想依赖springBoot可以使用java的单文件版MingWebServer
https://minglie.gitee.io/ming_api_mock/plugins/run_java/MingWebServer.java

## MingWebServer相关API
  ### 请求之前过滤器 
  begin(requet-> {});

  requet中包含请求内容
  ### 服务注册

``` java
  	g_functionMap.put("/postTest",(requet)->{
			renderUrl(requet,"https://www.baidu.com/");
			renderHtml(requet,"https://www.baidu.com/");
			//((Consumer<String>)req.get("sent")).accept("EEEEEEEEEEEEEEEEEEE");
			return "wwwwwww";
        });
 ```
   ### 响应json
    send(Map request,String res)
   ### 响应文本
    renderHtml(Map request,String res)
   ### 响应本地远程Url
    renderUrl(Map request,String url)
    
# java的单文件版执行
``` bash
curl https://minglie.gitee.io/ming_api_mock/plugins/run_java/MingWebServer.java > MingWebServer.java && javac  -encoding utf8 MingWebServer.java && java -Dfile.encoding=UTF-8 -cp ./ MingWebServer
```

# java的单文件版,加依赖jar执行
``` bash
@echo off
set classpath=.;%cd%/mingapi.jar;
javac -d . utils/*.java
javac -d . *.java
echo ===  running  ===
java com.github.minglie.autocode.App1
pause
```



