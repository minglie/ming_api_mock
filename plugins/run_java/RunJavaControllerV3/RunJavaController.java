


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.tools.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@RestController
@Configuration
@RequestMapping("/minglie/runjava")
@CrossOrigin
public class RunJavaController {

    @Autowired
    private Environment environment;

    @PostConstruct
    public void init() {
        System.out.println("runJava\n http://localhost:"+environment.getProperty("server.port")+"/minglie/runjava/index.html");

        System.out.println("look return\n http://localhost:"+environment.getProperty("server.port")+"/minglie/runjava/sse.html");


    }

    /**
     * 添加一个跨域过滤器
     * @return
     */
    @Bean
    public Filter corsFilter() {
      return   new Filter(){
          @Override
          public void doFilter(javax.servlet.ServletRequest request, javax.servlet.ServletResponse response, FilterChain chain) throws IOException, ServletException {
              HttpServletResponse res = (HttpServletResponse) response;
              HttpServletRequest req = (HttpServletRequest) request;
              String origin = req.getHeader("Origin");
              if (!org.springframework.util.StringUtils.isEmpty(origin)) {
                  //带cookie的时候，origin必须是全匹配，不能使用*
                  res.addHeader("Access-Control-Allow-Origin", origin);
              }
              res.addHeader("Access-Control-Allow-Methods", "*");
              String headers = req.getHeader("Access-Control-Request-Headers");
              // 支持所有自定义头
              if (!org.springframework.util.StringUtils.isEmpty(headers)) {
                  res.addHeader("Access-Control-Allow-Headers", headers);
              }
              res.addHeader("Access-Control-Max-Age", "3600");
              // enable cookie
              res.addHeader("Access-Control-Allow-Credentials", "true");
              chain.doFilter(request, response);
          }
      };
    }


    /**
     * 用于注入的钩子
     */
    public static Function<Object,Object> hook;
    static {
        hook=new Function<Object, Object>() {
            @Override
            public Object apply(Object o) {
                System.err.println(o.toString());
                return o;
            }
        };
    }

    static class CustomStringJavaCompiler {
        public static String classRegex = "class\\s+\\S+\\s+\\{";
        public static String packageRegex = "package\\s+\\S+\\s*;";
        //类全名
        private String fullClassName;
        private String sourceCode;
        //存放编译之后的字节码(key:类全名,value:编译之后输出的字节码)
        private Map<String, ByteJavaFileObject> javaFileObjectMap = new ConcurrentHashMap<>();
        //获取java的编译器
        private JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        //存放编译过程中输出的信息
        private DiagnosticCollector<JavaFileObject> diagnosticsCollector = new DiagnosticCollector<>();
        //执行结果（控制台输出的内容）
        private String runResult;
        //编译耗时(单位ms)
        private long compilerTakeTime;
        //运行耗时(单位ms)
        private long runTakeTime;
        public CustomStringJavaCompiler(String sourceCode) {
            this.sourceCode = sourceCode;
            this.fullClassName = getFullClassName(sourceCode);
        }
        /**
         * 编译字符串源代码,编译失败在 diagnosticsCollector 中获取提示信息
         * @return true:编译成功 false:编译失败
         */
        public boolean compiler() {
            long startTime = System.currentTimeMillis();
            StandardJavaFileManager standardFileManager = compiler.getStandardFileManager(diagnosticsCollector, null, null);
            JavaFileManager javaFileManager = new StringJavaFileManage(standardFileManager);

            JavaFileObject javaFileObject = new StringJavaFileObject(fullClassName, sourceCode);

            JavaCompiler.CompilationTask task = compiler.getTask(null, javaFileManager, diagnosticsCollector, null, null, Arrays.asList(javaFileObject));
            compilerTakeTime = System.currentTimeMillis() - startTime;
            return task.call();
        }
        /**
         * 执行main方法，重定向System.out.print
         */
        public OutputStream runMainMethod(Consumer<Object> consumer) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, UnsupportedEncodingException {
            PrintStream out = System.out;
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                long startTime = System.currentTimeMillis();
                PrintStream printStream=new PrintStream(System.out){
                    @Override
                    public void println(String x){
                        consumer.accept(x);
                    }
                };
                System.setOut(printStream);
                System.out.println("____编译耗时:"+compilerTakeTime+"ms");
                StringClassLoader scl = new StringClassLoader();
                Class<?> aClass = scl.findClass(fullClassName);
                Method main = aClass.getMethod("main", String[].class);
                Object[] pars = new Object[]{1};
                pars[0] = new String[]{};
                try {
                    main.invoke(null, pars); //调用main方法
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                //设置运行耗时
                runTakeTime = System.currentTimeMillis() - startTime;
                System.out.println("____运行耗时:"+runTakeTime+"ms");
                //设置打印输出的内容
            } finally {
                //还原默认打印的对象
                System.setOut(out);
            }
            return outputStream;
        }

        /**
         * @return 编译信息(错误 警告)
         */
        public String getCompilerMessage() {
            StringBuilder sb = new StringBuilder();
            List<Diagnostic<? extends JavaFileObject>> diagnostics = diagnosticsCollector.getDiagnostics();
            for (Diagnostic diagnostic : diagnostics) {
                sb.append(diagnostic.toString()).append("\r\n");
            }
            return sb.toString();
        }
        /**
         * 获取类的全名称
         *
         * @param sourceCode 源码
         * @return 类的全名称
         */
        public static String getFullClassName(String sourceCode) {
            String className = "";
            Pattern pattern = Pattern.compile(packageRegex);
            Matcher matcher = pattern.matcher(sourceCode);
            if (matcher.find()) {
                className = matcher.group().replaceFirst("package", "").replace(";", "").trim() + ".";
            }

            pattern = Pattern.compile(classRegex);
            matcher = pattern.matcher(sourceCode);
            if (matcher.find()) {
                className += matcher.group().replaceFirst("class", "").replace("{", "").trim();
            }
            return className;
        }

        /**
         * 自定义一个字符串的源码对象
         */
        private class StringJavaFileObject extends SimpleJavaFileObject {
            //等待编译的源码字段
            private String contents;

            //java源代码 => StringJavaFileObject对象 的时候使用
            public StringJavaFileObject(String className, String contents) {
                super(URI.create("string:///" + className.replaceAll("\\.", "/") + Kind.SOURCE.extension), Kind.SOURCE);
                this.contents = contents;
            }

            //字符串源码会调用该方法
            @Override
            public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
                return contents;
            }

        }

        /**
         * 自定义一个编译之后的字节码对象
         */
        private class ByteJavaFileObject extends SimpleJavaFileObject {
            //存放编译后的字节码
            private ByteArrayOutputStream outPutStream;

            public ByteJavaFileObject(String className, Kind kind) {
                super(URI.create("string:///" + className.replaceAll("\\.", "/") + Kind.SOURCE.extension), kind);
            }

            //StringJavaFileManage 编译之后的字节码输出会调用该方法（把字节码输出到outputStream）
            @Override
            public OutputStream openOutputStream() {
                outPutStream = new ByteArrayOutputStream();
                return outPutStream;
            }

            //在类加载器加载的时候需要用到
            public byte[] getCompiledBytes() {
                return outPutStream.toByteArray();
            }
        }

        /**
         * 自定义一个JavaFileManage来控制编译之后字节码的输出位置
         */
        private class StringJavaFileManage extends ForwardingJavaFileManager {
            StringJavaFileManage(JavaFileManager fileManager) {
                super(fileManager);
            }

            //获取输出的文件对象，它表示给定位置处指定类型的指定类。
            @Override
            public JavaFileObject getJavaFileForOutput(Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
                ByteJavaFileObject javaFileObject = new ByteJavaFileObject(className, kind);
                javaFileObjectMap.put(className, javaFileObject);
                return javaFileObject;
            }
        }

        /**
         * 自定义类加载器, 用来加载动态的字节码
         */
        private class StringClassLoader extends ClassLoader {
            @Override
            protected Class<?> findClass(String name) throws ClassNotFoundException {
                ByteJavaFileObject fileObject = javaFileObjectMap.get(name);
                if (fileObject != null) {
                    byte[] bytes = fileObject.getCompiledBytes();
                    return defineClass(name, bytes, 0, bytes.length);
                }
                try {
                    return ClassLoader.getSystemClassLoader().loadClass(name);
                } catch (Exception e) {
                    return super.findClass(name);
                }
            }
        }

        public static void runJava(String code,Consumer<Object> consumer){
            CustomStringJavaCompiler compiler = new CustomStringJavaCompiler(code);
            try {
                boolean res = compiler.compiler();
                if (res) {
                    try {
                        compiler.runMainMethod(consumer);
                    } catch (Exception e) {
                        consumer.accept(e.getMessage());
                    }
                } else {
                    consumer.accept( compiler.getCompilerMessage());
                    return ;
                }
            }catch (Exception e){
                consumer.accept(e.getMessage());
            }
        }
    }
    public static class CodeModel{
        String code;
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }
    }

    /**
     * 发送数据到客户端,找不到日志可以用它来打日志
     */
    public static PrintWriter ssePrintWriter;
    public static void sendSSE(String msg){
        try {
            if(ssePrintWriter!=null){
                RunJavaController.ssePrintWriter.write("event:slide\n");
                RunJavaController.ssePrintWriter.write("id: "+System.currentTimeMillis()+"\n");
                RunJavaController.ssePrintWriter.write("data: "+msg+"\n");
                RunJavaController.ssePrintWriter.write("retry: 10000\n");
                RunJavaController.ssePrintWriter.write("\n\n");
                RunJavaController.ssePrintWriter.flush();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 运行main函数
     * @param codeModel
     * @return
     */
    @PostMapping(value = "/runcode", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object runcode(@RequestBody CodeModel codeModel) {
        try {
            CustomStringJavaCompiler.runJava(codeModel.getCode(),(d)->{
                System.err.println(d);
                RunJavaController.sendSSE(d.toString());
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        return codeModel.getCode();
    }

    /**
     * 写sse测试
     * @param msg
     * @return
     */
    @GetMapping(value = "/ssewrite", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object ssewrite(String msg) {
        try {
            RunJavaController.sendSSE("qqqqqqqqqqqqq");
        }catch (Exception e){
            e.printStackTrace();
        }
        return "ok";
    }


    /**
     * sse 长连接响应打印
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping( value = "/sse",method = RequestMethod.GET)
    public void sse(HttpServletRequest request,
                    HttpServletResponse response) throws IOException {
        try {
            response.setHeader("Content-Type","text/event-stream");
            response.setHeader("Cache-Control","no-cache");
            response.setHeader("Connection","keep-alive");
            response.setCharacterEncoding("utf-8");
            System.err.println("SSEServer connect success");
            RunJavaController.ssePrintWriter= response.getWriter();
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                RunJavaController.sendSSE("____heart"+i+"");
                try {
                    TimeUnit.SECONDS.sleep(12000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
           e.printStackTrace();
        }
    }


    @RequestMapping( value = "/index.html",method = RequestMethod.GET)
    public void index(HttpServletRequest request,
                    HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        out.println(HttpUtils.indexHtml);
    }

    @RequestMapping( value = "/sse.html",method = RequestMethod.GET)
    public void sseIndex(HttpServletRequest request,
                      HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        out.println(HttpUtils.sseHtml);
    }

}


class HttpUtils{

    public static Map<String,String> globle_cacheMap=new HashMap<>();
    /**
     * Created by chengxia on 2018/12/4.
     */
    public static String post(String URL,String jsonStr){
        OutputStreamWriter out = null;
        BufferedReader in = null;
        StringBuilder result = new StringBuilder();
        HttpURLConnection conn = null;
        try{
            java.net.URL url = new URL(URL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            //发送POST请求必须设置为true
            conn.setDoOutput(true);
            conn.setDoInput(true);
            //设置连接超时时间和读取超时时间
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(10000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            //获取输出流
            out = new OutputStreamWriter(conn.getOutputStream());
            //向post请求发送json数据
            out.write(jsonStr);
            out.flush();
            out.close();
            //取得输入流，并使用Reader读取
            if (200 == conn.getResponseCode()){
                in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line;
                while ((line = in.readLine()) != null){
                    result.append(line);
                    System.out.println(line);
                }
            }else{
                System.out.println("ResponseCode is an error code:" + conn.getResponseCode());
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                if(out != null){
                    out.close();
                }
                if(in != null){
                    in.close();
                }
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
        return result.toString();
    }

    public static String get(String URL){
        HttpURLConnection conn = null;
        InputStream is = null;
        BufferedReader br = null;
        StringBuilder result = new StringBuilder();
        try{
            //创建远程url连接对象
            URL url = new URL(URL);
            //通过远程url连接对象打开一个连接，强转成HTTPURLConnection类
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            //设置连接超时时间和读取超时时间
            conn.setConnectTimeout(15000);
            conn.setReadTimeout(60000);
            conn.setRequestProperty("Accept", "application/json");
            //发送请求
            conn.connect();
            //通过conn取得输入流，并使用Reader读取
            if (200 == conn.getResponseCode()){
                is = conn.getInputStream();
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String line;
                while ((line = br.readLine()) != null){
                    result.append(line);
                }
            }else{
                System.out.println("ResponseCode is an error code:" + conn.getResponseCode());
            }
        }catch (MalformedURLException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                if(br != null){
                    br.close();
                }
                if(is != null){
                    is.close();
                }
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
            conn.disconnect();
        }
        return result.toString();
    }


    public static String getRemoteCacheByUrl(String url) {
        if(globle_cacheMap.containsKey(url)){
            return globle_cacheMap.get(url);
        }
        String text="";
        System.out.println("req remote url:"+url);
        if(url.startsWith("file:")){
            text=HttpUtils.readFile(new File(url.substring(5)));
        }else {
            text=HttpUtils.get(url);
        }
        globle_cacheMap.put(url,text);
        return text;
    }

    public static String readFile(File file) {
        InputStreamReader read = null;
        try {
            read = new InputStreamReader(new FileInputStream(file), "utf-8");
            BufferedReader bufferedReader = new BufferedReader(read);
            String lineTxt = null;
            StringBuffer buffer = new StringBuffer();
            String line = " ";
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }
            read.close();
            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String sseHtml="\n" +
            "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "    <meta charset=\"utf-8\">\n" +
            "    <title>runJava</title>\n" +
            "\n" +
            "    <script src=\"https://minglie.gitee.io/mingpage/static/js/M_mock.js\"></script>\n" +
            "    <style type=\"text/css\">\n" +
            "        body\n" +
            "        {\n" +
            "            background-color:black;\n" +
            "            color:green;\n" +
            "        }\n" +
            "    </style>\n" +
            "\n" +
            "</head>\n" +
            "<body>\n" +
            "<button id=\"cleanBtn\">clean</button>\n" +
            "<h1 id=\"connectId\">connect...</h1>\n" +
            "<div id=\"result\"></div>\n" +
            "\n" +
            "\n" +
            "<script>\n" +
            "    (function() {\n" +
            "\n" +
            "            M.EventSource('/minglie/runjava/sse',function(e){\n" +
            "                result.innerText+=e.data+ \"\\n\";\n"
            + "msg_end.click(); "+
            "            })\n" +
            "            cleanBtn.onclick=()=>result.innerText=\"\"\n" +
            "    })();\n" +
            "\n" +
            "\n" +
            "</script>\n" +
            "\n" +
            "</body>\n" +
            "<div><a id=\"msg_end\" name=\"1\" href=\"#1\">&nbsp</a></div>"+
            "</html>\n";



    public static String indexHtml="\n" +
            "\n" +
            "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
            "    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">\n" +
            "    <link rel=\"stylesheet\" href=\"https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css\">\n" +
            "    <script src=\"https://cdn.bootcss.com/jquery/3.3.1/jquery.js\"></script>\n" +
            "    <script src=\"https://minglie.gitee.io/ming_autotest/src/static/lib/monacoeditor/min/vs/loader.js\"></script>\n" +
            "    <script>\n" +
            "        M = {}\n" +
            "    </script>\n" +
            "    <style>\n" +
            "        * {\n" +
            "            touch-action: none;\n" +
            "        }\n" +
            "        #resize {\n" +
            "            width: 5px;\n" +
            "            height: 10px;\n" +
            "        }\n" +
            "        option {\n" +
            "            font-weight: bold;\n" +
            "            font-size: large;\n" +
            "            color: #00b4ef;\n" +
            "        }\n" +
            "\n" +
            "    </style>\n" +
            "</head>\n" +
            "\n" +
            "<body>\n" +
            "<div id=\"app\">\n" +
            "    <select id=\"laungeSelectId\" class=\"form-control\" style=\"width:50%; float: left;\" >\n" +
            "        <option>main.java</option>\n" +
            "    </select>\n" +
            "    <div align=\"center\">\n" +
            "        <button id=\"btn\" onclick=\"btnOnclick(this)\" style=\"float: left; width: 40%; height: 35px;\" align=\"center\" type=\"button\" class=\"btn btn-success btn-lg btn-block\">Run</button>\n" +
            "    </div>\n" +
            "    <select  id=\"themeSelectId\"  class=\"form-control\" style=\"width: 10%; float: right;\"  onchange=\"selectOnThemechang(this)\">\n" +
            "        <option>vs</option>\n" +
            "        <option>vs-dark</option>\n" +
            "        <option>hc-black</option>\n" +
            "    </select>\n" +
            "</div>\n" +
            "\n" +
            "<div id=\"container\" style=\"width:100%;height:2000px;float:left; border:1px solid grey\"></div>\n" +
            "<script>\n" +
            "\n" +
            "    M.languageMap={\n" +
            "        \"js\":\"javascript\",\n" +
            "        \"jsx\":\"javascript\",\n" +
            "        \"md\":\"markdown\",\n" +
            "        \"conf\":\"lua\"\n" +
            "    }\n" +
            "\n" +
            "    M.file =localStorage.file || \"server.js\"\n" +
            "    M.theme=localStorage.theme || \"vs-dark\";\n" +
            "    function getlanguage(file){\n" +
            "        M.language = file.split(\".\")[1];\n" +
            "        if(Object.keys(M.languageMap).indexOf(M.language)>=0){\n" +
            "            M.language=M.languageMap[M.language];\n" +
            "        }\n" +
            "        return   M.language;\n" +
            "    }\n" +
            "    require.config({\n" +
            "        baseUrl: 'https://minglie.gitee.io/ming_autotest/src/static/lib/monacoeditor/', paths: { 'vs': 'min/vs' }\n" +
            "    });\n" +
            "\n" +
            "\n" +
            "\n" +
            "    function selectOnThemechang(d){\n" +
            "        M.theme=d.value;\n" +
            "        localStorage.theme= M.theme;\n" +
            "        monaco.editor.setTheme(M.theme);\n" +
            "    }\n" +
            "\n" +
            "\n" +
            "    function ming_alert(str) {\n" +
            "        btn.innerHTML = str;\n" +
            "        window.setTimeout(() => {\n" +
            "            btn.innerHTML = \"Run\";\n" +
            "    }, 500);\n" +
            "    }\n" +
            "\n" +
            "    btnOnclick = function () {\n" +
            "        let fun = M.editor.getValue();\n" +
            "        localStorage.file=fun;\n" +
            "        $.ajax({\n" +
            "            type: \"post\",\n" +
            "            url: \"/minglie/runjava/runcode\",\n" +
            "            data: JSON.stringify( {\"code\": fun }),\n" +
            "            dataType: \"json\",\n" +
            "            contentType: \"application/json; charset=utf-8\",\n" +
            "            success: function (data) {\n" +
            "                ming_alert(data);\n" +
            "            },\n" +
            "            error: function (e) {\n" +
            "                ming_alert(JSON.stringify(e));\n" +
            "            }\n" +
            "        });\n" +
            "    }\n" +
            "\n" +
            "    require(['vs/editor/editor.main'], function () {\n" +
            "            var editor = monaco.editor.create(document.getElementById('container'), {\n" +
            "                value: [\n" +
            "                    localStorage.file\n" +
            "                ].join('\\n'),\n" +
            "                language: \"java\",\n" +
            "                theme: M.theme,\n" +
            "                automaticLayout: true,\n" +
            "                scrollbar: {\n" +
            "                    useShadows: false,\n" +
            "                    vertical: 'visible',\n" +
            "                    horizontal: 'visible',\n" +
            "                    horizontalSliderSize: 5,\n" +
            "                    verticalSliderSize: 5,\n" +
            "                    horizontalScrollbarSize: 15,\n" +
            "                    verticalScrollbarSize: 15,\n" +
            "                },\n" +
            "                quickSuggestions: true,\n" +
            "                overviewRulerBorder: true,\n" +
            "                minimap: {\n" +
            "                    enabled: false\n" +
            "                }\n" +
            "            });\n" +
            "            M.editor = editor;\n" +
            "            if( $(\"#themeSelectId\").val()!=M.theme){\n" +
            "                $(\"#themeSelectId\").val(M.theme)\n" +
            "                selectOnThemechang({ value: M.theme })\n" +
            "            }\n" +
            "        }\n" +
            "    );\n" +
            "\n" +
            "\n" +
            "</script>\n" +
            "</body>\n" +
            "\n" +
            "</html>";
}