package com.mingruncode.controller;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.tools.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@RestController
@Configuration
@RequestMapping("/minglie/runjava")
@CrossOrigin
public class RunJavaController {

    /**
     * 添加一个跨域过滤器
     * @return
     */
    @Bean
    public Filter corsFilter() {
      return   new Filter(){
          @Override
          public void doFilter(javax.servlet.ServletRequest request, javax.servlet.ServletResponse response, FilterChain chain) throws IOException, ServletException {
              HttpServletResponse res = (HttpServletResponse) response;
              HttpServletRequest req = (HttpServletRequest) request;
              String origin = req.getHeader("Origin");
              if (!org.springframework.util.StringUtils.isEmpty(origin)) {
                  //带cookie的时候，origin必须是全匹配，不能使用*
                  res.addHeader("Access-Control-Allow-Origin", origin);
              }
              res.addHeader("Access-Control-Allow-Methods", "*");
              String headers = req.getHeader("Access-Control-Request-Headers");
              // 支持所有自定义头
              if (!org.springframework.util.StringUtils.isEmpty(headers)) {
                  res.addHeader("Access-Control-Allow-Headers", headers);
              }
              res.addHeader("Access-Control-Max-Age", "3600");
              // enable cookie
              res.addHeader("Access-Control-Allow-Credentials", "true");
              chain.doFilter(request, response);
          }
      };
    }


    /**
     * 用于注入的钩子
     */
    public static Function<Object,Object> hook;
    static {
        hook=new Function<Object, Object>() {
            @Override
            public Object apply(Object o) {
                System.err.println(o.toString());
                return o;
            }
        };
    }

    static class CustomStringJavaCompiler {
        public static String classRegex = "class\\s+\\S+\\s+\\{";
        public static String packageRegex = "package\\s+\\S+\\s*;";
        //类全名
        private String fullClassName;
        private String sourceCode;
        //存放编译之后的字节码(key:类全名,value:编译之后输出的字节码)
        private Map<String, ByteJavaFileObject> javaFileObjectMap = new ConcurrentHashMap<>();
        //获取java的编译器
        private JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        //存放编译过程中输出的信息
        private DiagnosticCollector<JavaFileObject> diagnosticsCollector = new DiagnosticCollector<>();
        //执行结果（控制台输出的内容）
        private String runResult;
        //编译耗时(单位ms)
        private long compilerTakeTime;
        //运行耗时(单位ms)
        private long runTakeTime;
        public CustomStringJavaCompiler(String sourceCode) {
            this.sourceCode = sourceCode;
            this.fullClassName = getFullClassName(sourceCode);
        }
        /**
         * 编译字符串源代码,编译失败在 diagnosticsCollector 中获取提示信息
         * @return true:编译成功 false:编译失败
         */
        public boolean compiler() {
            long startTime = System.currentTimeMillis();
            StandardJavaFileManager standardFileManager = compiler.getStandardFileManager(diagnosticsCollector, null, null);
            JavaFileManager javaFileManager = new StringJavaFileManage(standardFileManager);

            JavaFileObject javaFileObject = new StringJavaFileObject(fullClassName, sourceCode);

            JavaCompiler.CompilationTask task = compiler.getTask(null, javaFileManager, diagnosticsCollector, null, null, Arrays.asList(javaFileObject));
            compilerTakeTime = System.currentTimeMillis() - startTime;
            return task.call();
        }
        /**
         * 执行main方法，重定向System.out.print
         */
        public OutputStream runMainMethod(Consumer<Object> consumer) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, UnsupportedEncodingException {
            PrintStream out = System.out;
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                long startTime = System.currentTimeMillis();
                PrintStream printStream=new PrintStream(System.out){
                    @Override
                    public void println(String x){
                        consumer.accept(x);
                    }
                };
                System.setOut(printStream);
                System.out.println("____编译耗时:"+compilerTakeTime+"ms");
                StringClassLoader scl = new StringClassLoader();
                Class<?> aClass = scl.findClass(fullClassName);
                Method main = aClass.getMethod("main", String[].class);
                Object[] pars = new Object[]{1};
                pars[0] = new String[]{};
                try {
                    main.invoke(null, pars); //调用main方法
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                //设置运行耗时
                runTakeTime = System.currentTimeMillis() - startTime;
                System.out.println("____运行耗时:"+runTakeTime+"ms");
                //设置打印输出的内容
            } finally {
                //还原默认打印的对象
                System.setOut(out);
            }
            return outputStream;
        }

        /**
         * @return 编译信息(错误 警告)
         */
        public String getCompilerMessage() {
            StringBuilder sb = new StringBuilder();
            List<Diagnostic<? extends JavaFileObject>> diagnostics = diagnosticsCollector.getDiagnostics();
            for (Diagnostic diagnostic : diagnostics) {
                sb.append(diagnostic.toString()).append("\r\n");
            }
            return sb.toString();
        }
        /**
         * 获取类的全名称
         *
         * @param sourceCode 源码
         * @return 类的全名称
         */
        public static String getFullClassName(String sourceCode) {
            String className = "";
            Pattern pattern = Pattern.compile(packageRegex);
            Matcher matcher = pattern.matcher(sourceCode);
            if (matcher.find()) {
                className = matcher.group().replaceFirst("package", "").replace(";", "").trim() + ".";
            }

            pattern = Pattern.compile(classRegex);
            matcher = pattern.matcher(sourceCode);
            if (matcher.find()) {
                className += matcher.group().replaceFirst("class", "").replace("{", "").trim();
            }
            return className;
        }

        /**
         * 自定义一个字符串的源码对象
         */
        private class StringJavaFileObject extends SimpleJavaFileObject {
            //等待编译的源码字段
            private String contents;

            //java源代码 => StringJavaFileObject对象 的时候使用
            public StringJavaFileObject(String className, String contents) {
                super(URI.create("string:///" + className.replaceAll("\\.", "/") + Kind.SOURCE.extension), Kind.SOURCE);
                this.contents = contents;
            }

            //字符串源码会调用该方法
            @Override
            public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
                return contents;
            }

        }

        /**
         * 自定义一个编译之后的字节码对象
         */
        private class ByteJavaFileObject extends SimpleJavaFileObject {
            //存放编译后的字节码
            private ByteArrayOutputStream outPutStream;

            public ByteJavaFileObject(String className, Kind kind) {
                super(URI.create("string:///" + className.replaceAll("\\.", "/") + Kind.SOURCE.extension), kind);
            }

            //StringJavaFileManage 编译之后的字节码输出会调用该方法（把字节码输出到outputStream）
            @Override
            public OutputStream openOutputStream() {
                outPutStream = new ByteArrayOutputStream();
                return outPutStream;
            }

            //在类加载器加载的时候需要用到
            public byte[] getCompiledBytes() {
                return outPutStream.toByteArray();
            }
        }

        /**
         * 自定义一个JavaFileManage来控制编译之后字节码的输出位置
         */
        private class StringJavaFileManage extends ForwardingJavaFileManager {
            StringJavaFileManage(JavaFileManager fileManager) {
                super(fileManager);
            }

            //获取输出的文件对象，它表示给定位置处指定类型的指定类。
            @Override
            public JavaFileObject getJavaFileForOutput(Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
                ByteJavaFileObject javaFileObject = new ByteJavaFileObject(className, kind);
                javaFileObjectMap.put(className, javaFileObject);
                return javaFileObject;
            }
        }

        /**
         * 自定义类加载器, 用来加载动态的字节码
         */
        private class StringClassLoader extends ClassLoader {
            @Override
            protected Class<?> findClass(String name) throws ClassNotFoundException {
                ByteJavaFileObject fileObject = javaFileObjectMap.get(name);
                if (fileObject != null) {
                    byte[] bytes = fileObject.getCompiledBytes();
                    return defineClass(name, bytes, 0, bytes.length);
                }
                try {
                    return ClassLoader.getSystemClassLoader().loadClass(name);
                } catch (Exception e) {
                    return super.findClass(name);
                }
            }
        }

        public static void runJava(String code,Consumer<Object> consumer){
            CustomStringJavaCompiler compiler = new CustomStringJavaCompiler(code);
            try {
                boolean res = compiler.compiler();
                if (res) {
                    try {
                        compiler.runMainMethod(consumer);
                    } catch (Exception e) {
                        consumer.accept(e.getMessage());
                    }
                } else {
                    consumer.accept( compiler.getCompilerMessage());
                    return ;
                }
            }catch (Exception e){
                consumer.accept(e.getMessage());
            }
        }
    }
    public static class CodeModel{
        String code;
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }
    }

    /**
     * 发送数据到客户端,找不到日志可以用它来打日志
     */
    public static PrintWriter ssePrintWriter;
    public static void sendSSE(String msg){
        try {
            if(ssePrintWriter!=null){
                RunJavaController.ssePrintWriter.write("event:slide\n");
                RunJavaController.ssePrintWriter.write("id: "+new Date().getTime()+"\n");
                RunJavaController.ssePrintWriter.write("data: "+msg+"\n");
                RunJavaController.ssePrintWriter.write("retry: 10000\n");
                RunJavaController.ssePrintWriter.write("\n\n");
                RunJavaController.ssePrintWriter.flush();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 运行main函数
     * @param codeModel
     * @return
     */
    @PostMapping(value = "/runcode", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object runcode(@RequestBody CodeModel codeModel) {
        try {
            CustomStringJavaCompiler.runJava(codeModel.getCode(),(d)->{
                System.err.println(d);
                RunJavaController.sendSSE(d.toString());
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        return codeModel.getCode();
    }

    /**
     * 写sse测试
     * @param msg
     * @return
     */
    @GetMapping(value = "/ssewrite", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object ssewrite(String msg) {
        try {
            RunJavaController.sendSSE("qqqqqqqqqqqqq");
        }catch (Exception e){
            e.printStackTrace();
        }
        return "ok";
    }


    /**
     * sse 长连接响应打印
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping( value = "/sse",method = RequestMethod.GET)
    public void sse(HttpServletRequest request,
                    HttpServletResponse response) throws IOException {
        try {
            response.setHeader("Content-Type","text/event-stream");
            response.setHeader("Cache-Control","no-cache");
            response.setHeader("Connection","keep-alive");
            response.setCharacterEncoding("utf-8");
            System.err.println("SSEServer connect success");
            RunJavaController.ssePrintWriter= response.getWriter();
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                RunJavaController.sendSSE("____heart"+i+"");
                try {
                    TimeUnit.SECONDS.sleep(12000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
           e.printStackTrace();
        }
    }



}