# 部分接口mock工具v1.1

 ming_mock_injection 的目的是将[ming_mock](https://www.npmjs.com/package/ming_mock)注入到浏览器,从而实现部分接口的mock,将mock这件事与原有项目彻底分离,

有下面几种方式

### x-switch方式注入
### 自定义谷歌插件注入
### [油猴插件](https://www.tampermonkey.net)注入


源码地址
https://gitee.com/minglie/ming_api_mock/tree/master/plugins/ming_mock_injection

当发现浏览器的title变为时间戳,就说明[ming_mock](https://www.npmjs.com/package/ming_mock)注入成功

# 目录结构
```
  |-- pageScripts
    |-- ajax_interceptor.js //为ming_mock.js外扩的一种ajax拦截方式
    |-- ming_mock_config.js //ming_mock.js 的配置文件
    |-- hook.js  //最终自己要注入的脚本
    |-- hook1.js  //最终自己要注入的脚本
    |-- ming_mock.js //ming_mock源码
    |-- pace.min.js  //x-switch 方式注入的的引子
 |--icon.png   //谷歌插件图标
 |--content.js //谷歌插件content_scripts 脚本
 |--manifest.json//谷歌插件配置文件
 |--page.js  //谷歌插件popup引入的js
 |--popup.js  //谷歌插件popup页面引入的js
 |--ming_api_mock.js //ming_api_mock源码
 |--popup.html  //谷歌插件popup页
 |--README.md   //帮助文档
 |--server.js   //x-switch 方式注入的服务文件
 |--sse.html  //非刷新注入脚本页面
 |--sse.js   //非刷新注入的脚本
```        


# 方式一  
 ## x-switch 注入ming_mock 

## x-switch 配置

https://www.fastmock.site/assets/js/pace.min.js 是我们注入脚本的引子,
一个常量js文件即可

```json
{
  "proxy": [
    [
      "https://www.fastmock.site/assets/js/pace.min.js",
      "http://localhost:8888/pace.min.js"
    ]
  ],

}
```

  如果安装了ming_mock_api直接执行
  ``` sh
  mock
  ```
  
  没安装则使用
  ``` javascript
node ming_api_mock.js
  ```
   
  可以通过手动修改/pageScripts/hook.js 来修改mock的数据,
  或访问 http://localhost:8888/sse.html  进行动态修改


## 方式二  
   ### 自定义谷歌插件 注入ming_mock 
   方式二是零配置的,只需把当前插件添加到谷歌扩展即可,修改pageScripts/hook.js来mock接口,
ming_mock_injection的目标是部分接口mock,如果做大规模mock,不如使用[ming_api_mock](https://gitee.com/minglie/ming_api_mock)
的其他相关插件[yuque_server](https://www.yuque.com/docs/share/fc8547e1-e815-4e50-817c-4829e3c76442?#4h0At)
或[ming_mockServer](https://github.com/minglie/ming_mockServer)

   谷歌插件开发参考
   https://www.cnblogs.com/liuxianan/p/chrome-plugin-develop.html
   ### ming_mock_injection类似工具
   https://github.com/YGYOOO/ajax-interceptor
   但我觉得mock的数据还是在编辑器里写方便,并且[ming_mock](https://www.npmjs.com/package/ming_mock)的express风格,持久化库,
  接口收参,就像在写后端一样,因此ming_mock_injection功能更强些

# 两种方式比较
  x-switch方式虽然依赖了ming_mock_api但能实现不刷新更新mock的接口,而自定义插件修改接口必须刷新浏览器才能生效

## 方式三  
  ### [油猴插件](https://www.tampermonkey.net)注入

ming_mock_injection 目的仅仅是将本地的hook.js与ming_mock.js注入浏览器,[油猴插件](https://www.tampermonkey.net)非常最适合做这种事
x-switch用来做脚本转换,油猴用来做脚本注入,两个插件要配合使用
仍然以fastmock为例
  ### 油猴脚本如下
  ```js

// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.fastmock.site/
// @require      file://D:\G\gitee\ming_api_mock\plugins\ming_mock_injection\pageScripts\ming_mock.js
// @require      file://D:\G\gitee\ming_api_mock\plugins\ming_mock_injection\pageScripts\hook1.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
})();
  ```







# 更新记录
 20200907 
 ming_mock.js 的ajax拦截当匹配到接口模版后,是不会发出真正的http请求的,大多少情况没问题,
 但对响应头有特殊要求的应用这种方式将无效, 因此添加一种拦截方式,收到响应后再拦截