const {Table, Button,Tooltip,Input,Icon,Modal,message,Switch} =antd;


window.global={};
window.MiApi_this ={}

global.maplist = (data) => {
    const databox = data.map((item) => {
        return Object.assign({}, item, {
            key: item.id
        })
    });
    return databox;
};



class MiApi extends React.Component {
    constructor(props) {
        super(props);
        this.columns = [
            {
                title: 'id',
                dataIndex: 'id',
                key: 'id',
                width: '5%',
                render: text => <a href="#"> { text } </a>
            },
            {
                title: 'api名称',
                dataIndex: 'name',
                key: 'name',
                width: '8%',
            }
        ];
        this.columns.push({
            title: '操作',
            key: 'operation',
            align: "center",
            render: (text, record) => {
                return <div>
                    <Tooltip title="编辑">
                        <Icon type="edit" onClick={this.showUpdateModal.bind(this,record)}/>
                    </Tooltip>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Tooltip title="删除">
                        <Icon type="delete" onClick={this.delete.bind(this,{id:record.id,parent_id:record.parent_id})}/>
                    </Tooltip>
                </div>;
            }
        });

        MiApi_this=this;

        this.state={
            record:{},
            Alldate:[],
            visible:false,
            total: 0,
            name:'',
            status:'2',
            startPage:1,
            limit:10,
            visible: false,
            modelType:"add",
            parent_id:props.parent_id
        };

    }
    componentDidMount() {
        this.flush(-1);
    };
    componentWillMount () {
       
    }
    componentWillUnmount(){
        this.setState = (state, callback) => {
            return
     }}
     flush(parent_id){
         if(parent_id){
            this.state.parent_id=parent_id;
         }

         axios.request({
            method: 'get',
            url: '/miApiListByPage',"params":{"startPage":1,"limit":5}
        }).then(d => {
            d=d.data;
            let action={
            type: "COM_ALLDATA",
            dataAll:d.rows,
            total: d.total
            }
            const Alldate =global.maplist(action.dataAll);
            const total = action.total;
            MiApi_this.setState({
                Alldate: Alldate,
                total: total
            })
        });

    //     M.IO.miApiListByPage({
    //         startPage: this.state.startPage,
    //         limit: this.state.limit,
    //         name:this.state.name,
    //         status:this.state.status,
    //         parent_id:parent_id
    //     }).then((d)=>{
    //        let action={
    //         type: "COM_ALLDATA",
    //         dataAll:d.rows,
    //         total: d.total
    //         }
    //         const Alldate =M.global.maplist(action.dataAll);
    //         const total = action.total;
    //         MiApi_this.setState({
    //             Alldate: Alldate,
    //             total: total
    //         })
    //     })

}
  
    addOrUpdate = () => {
        const { form } = this.formRef.props;
        form.validateFields((err, values) => {
                if (err) {
                    message.error("ng")
                    return;
                }else{
                    let values=form.getFieldsValue()
                    this.state.visible=false;
                    if(this.state.modelType=="add"){

                        // MIO.miApiAdd(values).then(d=>{
                        //     MiApi_this.flush();
                        //     message.success("ok")
                        // })

                        axios.request({
                            method: 'post',
                            url: '/miApiAdd',"data":values
                        }).then(d => {
                            MiApi_this.flush();
                            message.success("ok")
                        });

                    }else{

                        // MIO.miApiUpdate(values).then(d=>{
                        //     MiApi_this.flush();
                        //     message.success("ok")
                          
                        // })

                        axios.request({
                            method: 'post',
                            url: '/miApiUpdate',"data":values
                        }).then(d => {
                            MiApi_this.flush();
                            message.success("ok")
                        });


                    }
                }
        });
     
   };
    delete(r){
        // MIO.miApiDelete(r).then(d=>{
        //     MiApi_this.flush();
        //     message.success("ok")
        
        // })

        axios.request({
            method: 'get',
            url: '/miApiDelete',"params":{id:r.id}
        }).then(d => {
            MiApi_this.flush();
            message.success("ok")
        });

    }
    onTableChange(current, pageSize) {
        const state = this.state;
        state.startPage=current;
        state.limit=pageSize;
        MiApi_this.flush();
    }
    onRecordChange(text, record, checked){
        let f;
        true == checked ? f = 1 : f = 0;
        MiApi_this.flush();
    }
     showAddModal = (record) => {
        this.setState({
          visible: true,
          modelType:"add",
          record:{}
        });
      };
      showUpdateModal = (record) => {  
        const { form } = this.formRef.props;   
        form.resetFields();
        this.setState({
          visible: true,
          modelType:"update",
          record:record
        });
      };
      handleCancel = e => {
        const { form } = this.formRef.props;
        form.resetFields();
        this.setState({
          visible: false,
        });
      };
      searchData=e=>{
         const name=e.target.value;
         this.state.name=name
         MiApi_this.flush();
      }

    render() {
        return (
            <div>          
                <Input
                    type="text"
                    placeholder="name"
                    defaultValue={""}
                    onChange={this.searchData}
                    style={{ width: '30%', marginRight: '3%' }}
                />
          
                <Button type="primary" onClick={this.showAddModal}>添加api</Button>  <br/>  <br/>
                <Table dataSource={this.state.Alldate} columns={this.columns} pagination={false} />
                <br/><br/><br/>
               <MiApiForm   
                    title={"miApi"+this.state.modelType=="add"?"新增(parent_id|id)"+this.state.parent_id+"|"+this.state.record.id:"修改(parent_id|id)"+this.state.parent_id+"_"+this.state.record.id} 
                    wrappedComponentRef={(form) => this.formRef = form} 
                    visible={this.state.visible}
                    initData={this.state.record}
                    parent_id={this.state.parent_id}
                    onCancel={this.handleCancel}
                    onCreate={this.addOrUpdate}/>
                <antd.Pagination
                    showSizeChanger showQuickJumper
                    defaultCurrent={1}
                    total={this.state.total}
                    onChange={this.onTableChange.bind(this)}
                    onShowSizeChange={this.onTableChange.bind(this)}
                    pageSizeOptions={["5","10","20"]}
                />
            </div>
        )
    }
}



