const { Form, Select, Input, Button ,Radio} = antd;
const { Option } = Select;
const MiApiForm = Form.create({})(
  class extends React.Component {
    componentWillMount () {
     
     }
    render() {
      const { visible, onCancel, onCreate, form,title,initData,parent_id} = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title={title}
          okText="ok"
          onCancel={onCancel}
          onOk={onCreate}
        >
        <Form onSubmit={this.handleSubmit}>
        <Form.Item >
          {getFieldDecorator('parent_id', {
            initialValue:parent_id||initData.parent_id
          })(<Input hidden/>)}
        </Form.Item>
        <Form.Item >
          {getFieldDecorator('id', {
           initialValue:initData.id
          })(<Input hidden />)}
        </Form.Item>
        <Form.Item label="名称">
          {getFieldDecorator('name', {
            initialValue:initData.name,
            rules: [{ required: true, message: 'Please input your note!' }],
          })(<Input />)}
        </Form.Item>
      </Form>
        </Modal>
      );
    }
  },
);

