// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match         http://localhost:8888/static/index.html
// @require      https://minglie.gitee.io/mingpage/static/js/M_mock.js
// @grant        none
// ==/UserScript==

(function () {

    document.title = new Date().getTime();
    M.ajaxInterceptorEnable();
    M.api_prefix = `/mingapi/api`


    app.get("/miApiListByPage",function (req,res) {
        console.log(req.params.startPage,req.params.limit, "88")
        let r= M.listByPage(req.params.startPage,req.params.limit)
        console.log(r)
        res.send(r);
     });

})();