
app.begin(req=>{
    console.log("app.begin",req.url,req.params)
})

app.get("/miApiListByPage",function (req,res) {
    console.log(req.params.startPage,req.params.limit)
   r= M.listByPage(req.params.startPage,req.params.limit)
   console.log(r)
   res.send(JSON.stringify(r));
});


app.post("/miApiAdd",async function (req,res) {
    M.add(JSON.parse(req.body))
    res.send("ok")
});

app.post("/miApiUpdate",function (req,res) {
    M.update(JSON.parse(req.body))
    res.send("ok")
});


app.get("/miApiDelete",async function (req,res) {
    M.deleteById(req.params.id);
    res.send("ok")
});