// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
 // @match        *://*/*
// @grant        none
// ==/UserScript==

(function () {

  document.title = new Date().getTime();

async function registMockServer(config){

        console.log("ss",config)
      if(config.path=="system/user/list"){
          alert(44)

              return {"total":2,"rows":
                      [{"searchValue":null,"createBy":"admin","createTime":"2020-12-14 18:59:18","updateBy":null,"updateTime":null,"remark":"管理员1111","params":{},"userId":1,"deptId":103,"parentId":null,"roleId":null,"loginName":"admin","userName":"若依","userType":"00","email":"ry@163.com","phonenumber":"110","sex":"1","avatar":"","salt":"111111","status":"0","delFlag":"0","loginIp":"222.240.121.255","loginDate":"2021-01-25 23:12:15","pwdUpdateDate":null,"dept":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"deptId":103,"parentId":null,"ancestors":null,"deptName":"研发部门","orderNum":null,"leader":"若依","phone":null,"email":null,"status":null,"delFlag":null,"parentName":null},"roles":[],"roleIds":null,"postIds":null,"admin":true},{"searchValue":null,"createBy":"admin","createTime":"2020-12-14 18:59:18","updateBy":null,"updateTime":null,"remark":"测试员","params":{},"userId":2,"deptId":105,"parentId":null,"roleId":null,"loginName":"ry","userName":"若依","userType":"00","email":"ry@qq.com","phonenumber":"15666666666","sex":"1","avatar":"","salt":"222222","status":"0","delFlag":"0","loginIp":"222.175.121.187","loginDate":"2021-01-25 17:56:37","pwdUpdateDate":null,"dept":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"deptId":105,"parentId":null,"ancestors":null,"deptName":"测试部门","orderNum":null,"leader":"若依","phone":null,"email":null,"status":null,"delFlag":null,"parentName":null},"roles":[],"roleIds":null,"postIds":null,"admin":false}],"code":0,"msg":null};
      }


     return false;
}



let ajax_interceptor_qoweifjqon = {
  settings: {
  },
  originalXHR: window.XMLHttpRequest,
  myXHR: function() {
    const modifyResponse =async (d) => {
         let config={}
         config.path=this.responseURL.replace(/((http)|(https)):\/\/\S+?\/|(\?.*)/g, "")
         config.requestMethed=this.requestMethed;
         config.responseBody=this.responseText
         let txt=await registMockServer(config);
         if(txt){
            this.response=this.responseText = JSON.stringify(txt);
         }
    }
    const xhr = new ajax_interceptor_qoweifjqon.originalXHR;
    for (let attr in xhr) {
      if (attr === 'onreadystatechange') {
        xhr.onreadystatechange =async (...args) => {
          if (this.readyState == 4) {
             await  modifyResponse(args);
          }
          this.onreadystatechange && this.onreadystatechange.apply(this, args);
        }
        continue;
      } else if (attr === 'onload') {
        xhr.onload =async (...args) => {
          await modifyResponse(args);
          this.onload && this.onload.apply(this, args);
        }
        continue;
      }
      if (typeof xhr[attr] === 'function') {
        if(attr=="open"){
          this[attr] =(...args)=>{
            //console.log("aa",args)
            this.requestMethed=args[0];
            return xhr[attr].bind(xhr)(...args);
           }
        }else{
          this[attr] = xhr[attr].bind(xhr);
        }
      } else {
        // responseText和response不是writeable的，但拦截时需要修改它，所以修改就存储在this[`_${attr}`]上
        if (attr === 'responseText' || attr === 'response') {
          Object.defineProperty(this, attr, {
            get: () => this[`_${attr}`] == undefined ? xhr[attr] : this[`_${attr}`],
            set: (val) => this[`_${attr}`] = val,
            enumerable: true
          });
        } else {
          Object.defineProperty(this, attr, {
            get: () => xhr[attr],
            set: (val) => xhr[attr] = val,
            enumerable: true
          });
        }
      }
    }
  }
}
if(true){
  window.XMLHttpRequest = ajax_interceptor_qoweifjqon.myXHR;
}

})();