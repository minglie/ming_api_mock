# best_base_crud 是目前我认为的最佳mock方案

# 快捷稳定的方式1
   用[ModHeader插件](https://www.yuque.com/docs/share/cba30f00-2147-477f-a474-863fc9d351c9)重定向一下即可
     https://demo.ruoyi.vip/system/user/list  -> http://127.0.0.1:8888/system/user/list
     防止预检命令影响,方法直接写到app.begin里
    https://gitee.com/minglie/ming_api_mock/blob/master/plugins/ming_mock_injection/best_base_crud/modheaders/server.js
      或在app.begin额外处理一下
```js
    app.begin((req, res) => {
        console.log("====>", req.url,req.method)
        if(req.method=="OPTIONS"){ res.send({})}
    })
```
  [ModHeader插件](https://www.yuque.com/docs/share/cba30f00-2147-477f-a474-863fc9d351c9)转发的请求无法携带请求参数,需要用到请求参数可使用 [x-switch](https://www.yuque.com/docs/share/df931b4d-a137-4db3-b0cc-4fd9f44a9861)转发解决

```json
"proxy": [
    [
         "https://demo.ruoyi.vip/system/user/list(.*)",
         "http://localhost:8888/system/user/list$1"
    ]

```
 # 如果需要mock复杂接口,直接使用[ming_mock](https://www.npmjs.com/package/ming_mock)是最方便的,开启ajax拦截,node服务也省了


  

# 少量接口快捷优先
### ming_mock第一类拦截
https://gitee.com/minglie/ming_api_mock/blob/master/plugins/ming_mock_injection/best_base_crud/youhouway3/youhou_way3.js

### ming_mock第二类拦截
https://gitee.com/minglie/ming_api_mock/tree/master/plugins/ming_mock_injection/best_base_crud/youhouway4/youhou_way4.js



# 大量接口使用
## 服务端使用ming_api_mock 提供基本增,删,改,查,条件查询接口的mock
mock启动 server.js
   
```sh
 mock
```
## server.js
```javascript
M.api_prefix=`/mingapi/api`

app.post(`${M.api_prefix}/add`,(req,res)=>{
    r=M.add(JSON.parse(req.body));
    res.send(M.result(r));
});

app.get(`${M.api_prefix}/delete`,(req,res)=>{
    M.deleteById(req.params.id);
    res.send(M.result(`ok`));
});

app.post(`${M.api_prefix}/update`,(req,res)=>{
    r=M.update(JSON.parse(req.body));
    res.send(M.result(`ok`));
});

app.get(`${M.api_prefix}/getById`,(req,res)=>{
    r=M.getById(req.params.id);
    res.send(M.result(r));
});

app.get(`${M.api_prefix}/listAll`,(req,res)=>{
    r=M.listAll();
    res.send(M.result(r));
});

app.get(`${M.api_prefix}/listByParentId`,(req,res)=>{
    r=M.listByProp({parentId:req.params.parentId});
    res.send(M.result(r));
});

app.get(`${M.api_prefix}/listByPage`,(req,res)=>{
    r=M.listByPage(req.params.startPage,req.params.limit);
    res.send(M.result(r));
})

```



# 客户端使用[油猴插件](https://www.tampermonkey.net)注入ming_mock实现部分接口mock功能

## 方式1 直接在[油猴插件](https://www.tampermonkey.net)中编辑
```javascript 
// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://localhost:8000/
// @require      https://minglie.gitee.io/mingpage/static/js/M_mock.js
// @grant        none
// ==/UserScript==

(function () {

    document.title = new Date().getTime();
    M.ajaxInterceptorEnable();
    M.api_prefix = `/mingapi/api`

    
    app.post(`${M.api_prefix}/add`, (req, res) => {
        r = M.add(req.params);
        res.send(M.result(r));
    });

    app.get("/mingapi/api/listAll/", (req, res) => {
        console.log(req.params, "WWWWWW")
        res.send(M.result(M.listAll()));
    })

})();
```

## 方式2 在本地便捷器里编辑

```javascript 

// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://localhost:8000/
// @require      https://minglie.gitee.io/mingpage/static/js/M_mock.js
// @require      file://D:/G/gitee/ming_api_ui/server/hook.js
// @grant        none
// ==/UserScript==

(function() {

})();



```
## hook.js
```javascript

document.title = new Date().getTime();
M.ajaxInterceptorEnable();

if(1)
app.get("/mingapi/api/listAll/", (req, res) => {
        console.log(req.params,"WWWWWW")
        let r={"code":3002,"message":"操作成功","success":true,"data":[{"name":"wpf","id":"cy15905ro7e1600607347066"},{"name":"aa","id":"cqr7hppvwru1600607347669"},{"name":"aa","id":"pcq0w4yz3y71600607350789"}]}
        res.send(r)
})
```
