M.api_prefix=`/mingapi/api`

app.post(`${M.api_prefix}/add`,(req,res)=>{
    r=M.add(JSON.parse(req.body));
    res.send(M.result(r));
});

app.get(`${M.api_prefix}/delete`,(req,res)=>{
    M.deleteById(req.params.id);
    res.send(M.result(`ok`));
});

app.post(`${M.api_prefix}/update`,(req,res)=>{
    r=M.update(JSON.parse(req.body));
    res.send(M.result(`ok`));
});

app.get(`${M.api_prefix}/getById`,(req,res)=>{
    r=M.getById(req.params.id);
    res.send(M.result(r));
});

app.get(`${M.api_prefix}/listAll`,(req,res)=>{
    r=M.listAll();
    res.send(M.result(r));
});

app.get(`${M.api_prefix}/listByParentId`,(req,res)=>{
    r=M.listByProp({parentId:req.params.parentId});
    res.send(M.result(r));
});

app.get(`${M.api_prefix}/listByPage`,(req,res)=>{
    r=M.listByPage(req.params.startPage,req.params.limit);
    res.send(M.result(r));
})