app.begin((req,res)=>{
    console.log("req ",req.url)
    //如果请求/pace.min.js 则用  /pace.min.js + ./hook.js 两个文件之和来响应
     if(req.url.includes("/pace.min.js")){
            let paceJs=M.readFile("./pageScripts/pace.min.js") 
            let ming_mock=M.readFile("./pageScripts/ming_mock.js")
            let hook=M.readFile("./pageScripts/hook.js")
            res.writeHead(200, { "Content-Type": "text/html;charset='utf-8'" });
            res.write(paceJs+";"+ming_mock+";"+hook);
            res.end();
            res.alreadySend=true;
     }
})

M.beforeRun=(r)=>{
    //pace.min.js 与 hook.js 这两个js文件不需要运行
    if(r.endsWith("pace.min.js") || r.endsWith("hook.js")){
        return false;
    }else{
        return true;
    }  
};

if(Object.keys(M._get).indexOf("/sseServer/")==-1){
    M.sseApp=M.sseServer();
    app.get("/sseServer",M.sseApp)
 }
 
 
 app.post("/doSql",(req,res)=>{ 
      console.log(req.params.sql);
      M.writeFile(staticPath + "sse.js", req.params.sql);
      M.sseApp.send(req.params.sql.replace(/\n/g,""));
      res.send(M.result("ok"));
 })
 
 
 app.get("/a", (req, res) => {
     M.log(req.params)
     res.send("{}");
 })