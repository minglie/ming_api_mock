
M.findResPonseTemplate=function(options) {
    let path=M.urlToPath(options.url)
    let formatUrl=M.formatUrl(path)
    options.path=path;
    options.formatUrl=formatUrl;
    if(options.type=="POST"){
        if(Object.keys(app._post).indexOf(formatUrl)>-1){
            return  true
        }
    }else{
        if(Object.keys(app._get).indexOf(formatUrl)>-1){
            return  true
        }
    }
    return false;
}


 M.convertResPonseTemplate =function(item, options) {
        let data={}
        if(options.body){
            try{
                data=JSON.parse(options.body)
            }catch(e){
                data=  M.urlParse(options.body)
            }
        }
        options.data=data;
        if(options.type!="GET" && options.type!="POST"){
            options.type="GET";
        }
        return new Promise(
            function (reslove) {
                M.ajax({
                    url: options.path,
                    data: data,
                    type: options.type.toLocaleLowerCase(),
                    success: function (data) {
                        reslove(data)
                    }
                });
            }
        )
}

let ajax_interceptor_qoweifjqon = {
    settings: {
    },
    originalXHR: window.XMLHttpRequest,
    myXHR: function() {
      const modifyResponse =async () => {
           let config={}
           config.url=this.responseURL
           config.type="GET";
           let match=  M.findResPonseTemplate(config)
            if(match){
               let txt=await  M.convertResPonseTemplate(null,config)
                txt= M.endResponse(txt,config)
                this.responseText =txt;
                this.response = txt;
            }
      }
      
      const xhr = new ajax_interceptor_qoweifjqon.originalXHR;
      for (let attr in xhr) {
        if (attr === 'onreadystatechange') {
          xhr.onreadystatechange =async (...args) => {
            if (this.readyState == 4) {
               await  modifyResponse(args);
            }
            this.onreadystatechange && this.onreadystatechange.apply(this, args);
          }
          continue;
        } else if (attr === 'onload') {
          xhr.onload =async (...args) => {
            await modifyResponse(args);
            this.onload && this.onload.apply(this, args);
          }
          continue;
        }
        if (typeof xhr[attr] === 'function') {
          this[attr] = xhr[attr].bind(xhr);
        } else {
          // responseText和response不是writeable的，但拦截时需要修改它，所以修改就存储在this[`_${attr}`]上
          if (attr === 'responseText' || attr === 'response') {
            Object.defineProperty(this, attr, {
              get: () => this[`_${attr}`] == undefined ? xhr[attr] : this[`_${attr}`],
              set: (val) => this[`_${attr}`] = val,
              enumerable: true
            });
          } else {
            Object.defineProperty(this, attr, {
              get: () => xhr[attr],
              set: (val) => xhr[attr] = val,
              enumerable: true
            });
          }
        }
      }
    },
  
    originalFetch: window.fetch.bind(window),
    myFetch: async function(...args) {
        if(M.beforeSend(args)==false){
            return;
        }
        return ajax_interceptor_qoweifjqon.originalFetch(...args).then(async (response) => {
            let txt = undefined;
            let [reqPath,otherConfig]=args;
            reqConfig={}
            reqConfig.url=reqPath;
            reqConfig.type=reqConfig.method;
            let matched = M.findResPonseTemplate(reqConfig);
            if (matched) {
                txt=await  M.convertResPonseTemplate(null,reqConfig);
            }
          txt= M.endResponse(txt,response)
          if (txt !== undefined) {
            const stream = new ReadableStream({
              start(controller) {
                const bufView = new Uint8Array(new ArrayBuffer(txt.length));
                for (var i = 0; i < txt.length; i++) {
                  bufView[i] = txt.charCodeAt(i);
                }
                controller.enqueue(bufView);
                controller.close();
              }
            });
            const newResponse = new Response(stream, {
              headers: response.headers,
              status: response.status,
              statusText: response.statusText,
            });
            const proxy = new Proxy(newResponse, {
              get: function(target, name){
                switch(name) {
                  case 'ok':
                  case 'redirected':
                  case 'type':
                  case 'url':
                  case 'useFinalURL':
                  case 'body':
                  case 'bodyUsed':
                    return response[name];
                }
                return target[name];
              }
            });
            for (let key in proxy) {
              if (typeof proxy[key] === 'function') {
                proxy[key] = proxy[key].bind(newResponse);
              }
            }
            return proxy;
          } else {
            return response;
          }
        })}   
}



if(M.ajax_interceptor_qoweifjqon_enable){
    window.XMLHttpRequest = ajax_interceptor_qoweifjqon.myXHR;
    window.fetch = ajax_interceptor_qoweifjqon.myFetch;
}


