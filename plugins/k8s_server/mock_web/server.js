
//可以在服务启动后再修改为mock_db的service
M.mock_db_service="http://localhost:9999"

/**
 * http://localhost:8888/mock_web/register?username=zs&paasword=123456
 * 注册
 */
app.get("/mock_web/register",async (req,res)=>{
     let {username,password}=req.params; 
     M.require(`${M.mock_db_service}/mock_db/user/add?username=${username}&password=${password}`).then(d=>{
         res.send(M.result(d))
     })
})


/**
 *  http://localhost:8888/mock_web/login?username=zs&paasword=123456
 * 登陆
 */
app.get("/mock_web/login",async (req,res)=>{
     let {username,password}=req.params; 
     M.require(`${M.mock_db_service}/mock_db/user/queryByUseruame?username=${username}`).then(d=>{
         res.send(M.result(d))
     })
})



app.begin((req, res) => {
    M.log("req ",req.url)
})


