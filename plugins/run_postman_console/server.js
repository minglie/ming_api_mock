app.post("/_run_", async (req, res) => {
    try {
        if(M.beforeWriteFile(req.url)){
             //默认提供一个__default_server.js不必存储
            if(req.params.file.endsWith("__default_server.js")){
                M.__default_server_js=req.params.fun;
            }else{
                M.writeFile(staticPath + req.params.file, req.params.fun);
            }
        }
        if( M.beforeRun(req.url))
        {
            if (req.params.file.endsWith(".js")) {
                 if(req.params.file !="server.js"){
                    req.params.fun= req.params.fun.replace(/require\(/g,"require(staticPath+'node_modules/'+")
                    req.params.fun= req.params.fun.replace(/console.log/g,"M.console_log")
                 }
                 eval(req.params.fun)
            }
        }
        res.send(M.result("ok"))
        M.endRun(req.params.file);
    } catch (e) {
        console.error(e)
        res.send(M.result("error", false))
    }
})

sseApp=M.sseServer()
app.get("/sseServer",sseApp)

M.console_log=function (...message) {
        console.log(...message);
        sseApp.send(message[0]);
}