//M=require("ming_node")

M.reqComHeaders={"cookie": "lang=zh-cn; UM_distinctid=17164e5002a1e0-0edf34577e9973-591c3314-1fa400-17164e5002b82b; yuque_ctoken=UIbNQxGhDFdDfOdiPWJLV6RE; _TRACERT_COOKIE__SESSION=9203414b-b4a8-4e55-aaf4-5ccb5b3ce33c; _yuque_session=iSlIakErHXv6luf6SgVG8K8fzRx02qaaUtciMQGBIiJdigabSifz_sTjXAFhMNBh4MMpA_RrPNY_YDUlhfCGVQ==; CNZZDATA1272061571=1200705279-1586527421-%7C1596971524; tree=a385%018bca55c3-637b-4b81-8a23-21d875c81634%0168"}

M.yuqueDocApi="https://www.yuque.com/ffw1nf/tksb3d/eyf6g7/markdown?attachment=true&latexcode=false&anchor=false&linebreak=false";


if(!M.yuqueServerObj){
    M.yuqueServerObj={"/test":"{}"}
}

 function registYuqueServer(){
    return  new Promise((resolve, reject) => {
            M.get(M.yuqueDocApi).then(s=>{   
                  let s1=s.split(/\`\`\`/g)
                  let resultArr=[]
                  for(let i=0;i<s1.length;i++){
                  if(i>0 && s1[i].startsWith("json\n") && s1[i-1].endsWith("responseBody\n")){
                        resultArr.push(s1[i].replace("json\n",""))
                  }
                  
                  }
                  resultArr=resultArr.map(u=>u.replace(/\/\/(.+)+/g,""))
                  //console.log(resultArr[0])
                  let text=s;
                  text = text.replace(/"|'|`/g, "");
                  text = text.replace(/ +/g, " ");
                  let arr = text.split(/[\r\n]/g);
                  let pathList=[]
                  for(let i=0;i<arr.length;i++){
                  if(arr[i].trim().startsWith("GET ") || arr[i].trim().startsWith("POST ")){
                        pathList.push(arr[i].replace(/GET|POST/g,"").trim());
                  }
                  }
                  for(let i=0;i<arr.length;i++){
                       M.yuqueServerObj[pathList[i]]=resultArr[i];
                  }  
                  resolve(M.yuqueServerObj)  
      })
  })
}

registYuqueServer().then(u=>console.log(u))

app.begin((req, res) => {
      console.log("req ", req.url)
      var pathname = url_module.parse(req.url).pathname;
      if (Object.keys(M.yuqueServerObj).indexOf(pathname)>-1) {
          res.send(M.yuqueServerObj[pathname])
      }
  })
