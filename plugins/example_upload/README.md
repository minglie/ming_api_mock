#### 目的
api-mock文件上传下载插件,实现计算机之间双向的大文件传输

# server.js

``` 安装
 mock https://minglie.gitee.io/ming_api_mock/plugins/example_upload/test_case/server.js
```

``` js
+async function(){
    await M.require("https://gitee.com/minglie/ming_api_mock/raw/master/plugins/example_upload/server.js");
    M.__default_file["__default_index.html"]=await M.get("https://gitee.com/minglie/ming_api_mock/raw/master/plugins/example_upload/index.html");
    console.log("show on")
    console.log("http://localhost:8888/__default_index.html")  
}();
```

#  bash上传
``` bash
curl --location --request POST 'http://minglie2.vaiwan.com/fileupload' --form 'file=@"/root/a.yaml"'
```

# bash下载
``` bash
 curl  http://minglie2.vaiwan.com/a.yaml >  b.yaml
```
