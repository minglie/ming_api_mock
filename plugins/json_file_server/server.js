
let s={}

/**
  用户添加
 ｛
      "name":"zs"
  ｝
 */
s["/user/add"]={"name":"zs"}


/**
    用户查询
   ｛
       "name":"zs"
    ｝
 */
s["/user/listByPage"]={"name":"ls"}


app.begin((req, res) => {
    console.log("req ", req.url)
    var pathname = url_module.parse(req.url).pathname;
    if (Object.keys(s).indexOf(pathname)>-1) {
        res.send(JSON.stringify(s[pathname]))
    }
})