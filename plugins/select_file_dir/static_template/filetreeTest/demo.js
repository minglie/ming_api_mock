Vue.component('item', {
    template: '#item-template',
    props: {
      model: Object
    },
    data: function () {
      return {
        open: false
      }
    },
    computed: {
      isFolder: function () {
        return this.model.children &&
          this.model.children.length
      }
    },
    methods: {
      toggle: function () {
        if (this.isFolder) {
          this.open = !this.open
         console.log(this.model.value)

            fetch("/staticPath?staticPath="+this.model.value, {
              method: 'GET',
              mode: 'cors'
          }
          ).then((res) => {
              return res.json()
          }).then((res) =>document.title=res.data).catch((error) => {
              console.error(error)
          });

        }
        else
        {
          alert(this.model.value);
        }
      },
      changeType: function () {
        if (!this.isFolder) {
          Vue.set(this.model, 'children', [])
          this.addChild()
          this.open = true
        }
      },
      addChild: function () {
        this.model.children.push({
          name: 'new stuff'
        })
      },
      openFolder:function(){
  
      }
    }
  })
  
  // boot up the demo
  var demo = new Vue({
    el: '#demo',
    data: {
      treeData: data
    }
  })