﻿staticPath1="D:/G/test/output/static"
M.remoteStaticPath="https://minglie.gitee.io/mingpage";


var fs = require('fs')

function geFileList(path)
{
   var filesList = [];
   var targetObj = {};
   readFile(path,filesList,targetObj);
   return filesList;
}

//遍历读取文件
function readFile(path,filesList,targetObj)
{
   files = fs.readdirSync(path);//需要用到同步读取
   files.forEach(walk);
   function walk(file)
   {  
        states = fs.statSync(path+'/'+file);         
        if(states.isDirectory())
        {
            var item ;
            if(targetObj["children"])
            {
                item ={name:file,children:[],value:path+'/'+file};
                targetObj["children"].push(item);
            }
            else
            {
               item = {name:file,children:[],value:path+'/'+file};
               filesList.push(item);
            }

            readFile(path+'/'+file,filesList,item);
        }
        else
        {   
            //创建一个对象保存信息
            var obj = new Object();
            obj.size = states.size;//文件大小，以字节为单位
            obj.name = file;//文件名
            obj.path = path+'/'+file; //文件绝对路径

            if(targetObj["children"])
            {
               var item = {name:file,value:obj.path}
               targetObj["children"].push(item);
            }
            else
            {
                var item = {name:file,value:obj.path};
                filesList.push(item);
            }
        }     
    }
}
//写入文件utf-8格式
function writeFile(fileName,data)
{  
  fs.writeFile(fileName,data,'utf-8',complete);
  function complete()
  {
     console.log("文件生成成功");
  } 
}
var filesList = geFileList(staticPath1);
let treeJson={"name":staticPath1,value:staticPath1,children:filesList}
//writeFile("tree.js","var data="+JSON.stringify(treeJson));
let tree_js="var data="+JSON.stringify(treeJson);


let s={}

s["/index.html"]=async (req,res)=>{
    await  res.renderUrl("https://6d69-minglie-31e331-1302367385.tcb.qcloud.la/dh/select.html")
}
s["/demo.js"]=async (req,res)=>{
   await res.renderUrl("https://gitee.com/minglie/ming_api_mock/raw/master/plugins/select_file_dir/static_template/filetreeShow/demo.js");
}

s["/tree.js"]=(req,res)=>{
      res.renderJs(tree_js)
}


app.begin(async (req, res) => {
    console.log("req ", req.url)
    var pathname = url_module.parse(req.url).pathname;
    if (Object.keys(s).indexOf(pathname)>-1) {
       await  s[pathname](req,res)
    }
})


app.get("/staticPath1",(req,res)=>{
    staticPath1=req.params.staticPath1;
    app.set("views", staticPath1);
    res.send(M.result("ok"))
}) 


app.get("/getFile",(req,res)=>{
  let file=req.params.file;
  let r= M.readFile(file)
  res.send(M.result(r))
}) 


