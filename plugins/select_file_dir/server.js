staticPath1=staticPath;

console.log("ReadMe: https://gitee.com/minglie/ming_api_mock/tree/master/plugins/select_file_dir")


var filesList = M.getFileList(staticPath1);
let treeJson={"name":staticPath1,value:staticPath1,children:filesList}
let tree_js="var data="+JSON.stringify(treeJson);

let index_html=`
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>select dir</title>
        <script src="https://unpkg.com/vue/dist/vue.js"></script>
        <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
        <script src="https://minglie.gitee.io/ming_autotest/src/static/lib/monacoeditor/min/vs/loader.js"></script>
        <script>
            M = {}
        </script>
        <script src="https://minglie.gitee.io/mingpage/static/smalltool/share_edit/static/launge_default_value.js"></script>
    </head>
    <style>
    body {
          font-family: Menlo, Consolas, monospace;
          color: #444;
        }
        .item {
          cursor: pointer;
        }
        .bold {
          font-weight: bold;
        }
        ul {
          padding-left: 13px;
          line-height: 1.5em;
          list-style-type:none;
        }
        img{
            width: 15px;
            height: 15px;
        }
        #box{
            width:100%;
            height:100%;
            overflow:hidden;
        }
        #left{
            width:25%;
            height:10%;
            float:left;
        }

        #right{
            float:right;
            width:75%;
            height:10000px;
        }
    </style>
<body>

<script>
      require.config({
            baseUrl: 'https://minglie.gitee.io/ming_autotest/src/static/lib/monacoeditor/', paths: { 'vs': 'min/vs' }
        });



M.languageMap={
    "jsx":"javascript",
    "js":"javascript",
    "md":"markdown",
    "conf":"lua"
}

function getlanguage(file){
        M.language = file.split(".")[1];
        if(Object.keys(M.languageMap).indexOf(M.language)>=0){
              M.language=M.languageMap[M.language];
        }
        return   M.language;
}

function mingload(language,content){

  $("#right").children().remove();
    
    require(['vs/editor/editor.main'], function() {
      
          var editor = monaco.editor.create(document.getElementById('right'), {
              value:content,
              language: language,
              theme:'vs-dark',
              automaticLayout:true,
              scrollbar: {
                  useShadows:false,
                  vertical:'visible',
                  horizontal:'visible',
                  horizontalSliderSize:5,
                  verticalSliderSize:5,
                  horizontalScrollbarSize:15,
                  verticalScrollbarSize:15,
              },
              quickSuggestions:true,
              overviewRulerBorder:true,
              minimap: {
                  enabled:false
              }
          });

          M.editor=editor;
      });
}

mingload('javascript',"sss")
   
</script>
</script>






<!-- item template -->
<script type="text/x-template" id="item-template">
  <li>
    <div  :class="{bold: isFolder}"
      @click="toggle"
      @dblclick="changeType">
      <img :src="isFolder ? 'https://ming-bucket-01.oss-cn-beijing.aliyuncs.com/static/img/folder.png':'https://ming-bucket-01.oss-cn-beijing.aliyuncs.com/static/img/file.png'"/>
         {{model.name}}
    </div>

    <ul v-show="open" v-if="isFolder">      
      <item   class="item"
        v-for="model in model.children"
        :model="model">
      </item>
    </ul>

  </li>
</script>

<div id="box">

    <div id="left">
      <ul id="demo">
        <item
          class="item"
          :model="treeData">
        </item>
      </ul>
    </div>

      <div id="right">
      </div>
</div>

</body>
   
    <script src="tree.js"></script>
    <script type="text/javascript" src="demo.js"></script>
</html>

`;


let demo_js=`
Vue.component('item', {
  template: '#item-template',
  props: {
    model: Object
  },
  data: function () {
    return {
      open: false
    }
  },
  computed: {
    isFolder: function () {
      return this.model.children &&
        this.model.children.length
    }
  },
  methods: {
    toggle: function () {
      if (this.isFolder) {
          this.open = !this.open
          console.log(this.model.value)
          if(0)
          fetch("/staticPath1?staticPath1="+this.model.value, {
                method: 'GET',
                mode: 'cors'
            }
            ).then((res) => {
                return res.json()
            }).then((res) =>document.title=res.data).catch((error) => {
                console.error(error)
            });
      }else
      {
         let file=this.model.value;
         fetch("/getFile?file="+file, {
              method: 'GET',
              mode: 'cors'
          }
          ).then((res) => {
              return res.json()
          }).then((res) =>{
            mingload(getlanguage(file),res.data)
            console.log(res.data)
          }).catch((error) => {
              console.error(error)
          });
      }
    },
    changeType: function () {
      if (!this.isFolder) {
        Vue.set(this.model, 'children', [])
        this.addChild()
        this.open = true
      }
    },
    addChild: function () {
      // this.model.children.push({
      //   name: 'new stuff'
      // })
    },
    openFolder:function(){

    }
  }
})

// boot up the demo
var demo = new Vue({
  el: '#demo',
  data: {
    treeData: data
  }
})
`
let s={}

s["/index.html"]=(req,res)=>{
    res.writeHead(200, { "Content-Type": "text/html;charset='utf-8'" });
    res.write(index_html);
    res.end();
    res.alreadySend = true;
}
s["/demo.js"]=(req,res)=>{
    res.writeHead(200, { "Content-Type":"application/javascript" });
    res.write(demo_js);
    res.end();
    res.alreadySend = true;
}

s["/tree.js"]=(req,res)=>{
    res.writeHead(200, { "Content-Type":"application/javascript" });
    res.write(tree_js);
    res.end();
    res.alreadySend = true;
}





app.begin((req, res) => {
    console.log("req ", req.url)
    var pathname = url_module.parse(req.url).pathname;
    if (Object.keys(s).indexOf(pathname)>-1) {
        s[pathname](req,res)
    }
})


app.get("/staticPath1",(req,res)=>{
    staticPath1=req.params.staticPath1;
    app.set("views", staticPath1);
    res.send(M.result("ok"))
}) 


app.get("/getFile",(req,res)=>{
  let file=req.params.file;
  let r= M.readFile(file)
  res.send(M.result(r))
}) 


