
M.proxyHost="http://127.0.0.1:9999"

//启用静态资源代理
M.enableProxyStatucResource=false;

//忽略代理的地址
ignoreUrlList=["/","/favicon.ico"]

app.begin(async (req,res)=>{
    if(ignoreUrlList.indexOf(req.url)>-1||req.url.startsWith("/_")||req.url.startsWith("/.")){
        return;
    }
    if(req.isStaticRequest()){
        if(M.enableProxyStatucResource){
            res.renderUrl(M.proxyHost+req.url);
        }
        return;
    }
    res.alreadySend = true;
    //转换为axios格式
    let axiosConfig=await M.getAxiosConfig(req);
    console.log("====>",JSON.stringify(axiosConfig))
    //发出请求
    let result=await M.axios(axiosConfig)
    console.log("<======",result)
    console.log("---------------------------")
    res.send(result);
})












M.getAxiosConfig=async (req)=>{
    return new Promise((resolve,reject) => {
    let axiosConfig={}
    axiosConfig.url=M.proxyHost+req.url
    axiosConfig.method=req.method.toLocaleLowerCase();
    axiosConfig.headers=req.headers
    let postStr = '';
    req.on('data', function (chunk) {
        postStr += chunk;  
    })
    req.on('end', function (err, chunk) {
        req.body = postStr;  /*表示拿到post的值*/
        postData = "";
        try {
            if(req.body.indexOf("=")==-1){
                postData = JSON.parse(req.body);
            }else{
                postData = url_module.parse("?" + req.body, true).query;
            }
        } catch (e) {
        }
        axiosConfig.data=postData
        resolve(axiosConfig)
     })
   })
}


M.axios= function (axiosConfig) {
    let bodyObj = axiosConfig;
    return new Promise((r, j) => {
        try {
            let getData = {}
            if (bodyObj.method == "post") {
                if (bodyObj.params) {
                    let getData = Object.keys(bodyObj.params).map(u => u + "=" + bodyObj.params[u]).join("&");
                    if (bodyObj.url.indexOf("?") > 0) {
                        getData = "&" + getData;
                    } else {
                        getData = "?" + getData;
                    }
                    bodyObj.url = bodyObj.url + getData;
                }
                if(bodyObj.headers['content-type'].startsWith('application/json')){
                    M.postJson(bodyObj.url, r, bodyObj.data,bodyObj.headers)
                }else{
                    M.post(bodyObj.url, r, bodyObj.data,bodyObj.headers)
                }
            } else {
                M.get(bodyObj.url, r, bodyObj.params, bodyObj.headers)
            }
        } catch (e) {
            j(e);
        }
    });
}