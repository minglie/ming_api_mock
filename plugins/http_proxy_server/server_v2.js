
M.proxyHost="https://www.fastmock.site"

//启用静态资源代理
M.enableProxyStatucResource=false;

//忽略代理的地址
ignoreUrlList=["/","/favicon.ico"]

app.begin(async (req,res)=>{
    if(ignoreUrlList.indexOf(req.url)>-1||req.url.startsWith("/_")||req.url.startsWith("/.")){
        return;
    }
    if(req.isStaticRequest()){
        if(M.enableProxyStatucResource){
            res.renderUrl(M.proxyHost+req.url);
        }
        return;
    }
    res.alreadySend = true;
    //转换为axios格式
    let axiosConfig=await M.getAxiosConfig(req);
    console.log("====>",JSON.stringify(axiosConfig))
    //发出请求
    let result=await M.axios(axiosConfig)
    console.log("<======",result)
    console.log("---------------------------")
    res.send(result);
})





M.getAxiosConfig=async (req)=>{
    return new Promise((resolve,reject) => {
    let axiosConfig={}
    axiosConfig.url=M.proxyHost+req.url
    axiosConfig.method=req.method.toLocaleLowerCase();
    axiosConfig.headers=req.headers
    let postStr = '';
    req.on('data', function (chunk) {
        postStr += chunk;  
    })
    req.on('end', function (err, chunk) {
        req.body = postStr;  /*表示拿到post的值*/
        postData = "";
        try {
            if(req.body.indexOf("=")==-1){
                postData = JSON.parse(req.body);
            }else{
                postData = url_module.parse("?" + req.body, true).query;
            }
        } catch (e) {
        }
        axiosConfig.data=postData;
        axiosConfig.body=req.body;
        resolve(axiosConfig)
     })
   })
}






M.axios = function (axiosConfig) {
    axiosConfig.headers.host="";
    var urlObj = url_module.parse(axiosConfig.url)
    var options = {
        hostname: urlObj.hostname,
        port: urlObj.port,
        path: urlObj.path,
        method: axiosConfig.method.toLocaleUpperCase(),
        headers: axiosConfig.headers
    }
    let reqHttp = http;
    if (axiosConfig.url.startsWith("https")) {
        reqHttp = https;
    }
    var html = '';
    return new Promise((resolve, reject) => {
        var req = reqHttp.request(options, function (res) {
            options = M.httpBefore(options);
            if (options == false) {
                return;
            }
            res.setEncoding('utf-8');
            res.on('data', function (chunk) {
                html += chunk;
            });
            res.on('end', function () {
                M.httpEnd(html);
                resolve(html);
            });

        });
        req.on('error', function (err) {
            console.error(err);
        });
        req.write(axiosConfig.body);
        req.end();
    })
}