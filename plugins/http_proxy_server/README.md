
#### 目的

这个插件的目的代理服务器

#### server_v1.js
将数据转换为axios标准格式后再调ming_node中的httpClient,仅支持GET,POST


#### server_v2.js
将数据转换为原始body,支持各种请求头

# 插件安装
``` bash
mock https://minglie.gitee.io/ming_api_mock/plugins/http_proxy_server/server.js 9999
```
