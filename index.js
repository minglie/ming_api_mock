M = require("ming_node")

var os = require('os');
var args = process.argv.splice(2)
let argsPath=args[0] || "./";
let port= args[1] ||8888;
M.__default_file={};
M.__default_file["__default_server.js"]="console.log(1)";
M.__default_file["__default_index.html"]="hello";
staticPath=argsPath.replace(/\\/g,"/") || "./" ;
console.log("static path="+staticPath)
var app = M.server();
app.listen(port);
app.set("views", staticPath);
M.log_path = staticPath+"M.log";
M.map_path =staticPath+ "M_map.json";
M.database_path = staticPath+"M_database.json";
M.endRun=()=>{};
M.beforeRun=()=>{return true};
M.beforeWriteFile=()=>{return true};
app.get("/", async (req, res) => {
    res.writeHead(200, { "Content-Type": "text/html;charset='utf-8'" });
    res.write(indexHtml);
    res.end();
})


app.post("/_run_", async (req, res) => {
    try {
        if(M.beforeWriteFile(req.url)){
             //默认__default_不必存储
            if(req.params.file.startsWith("__default_")){
                M.__default_file[req.params.file]=req.params.fun;
            }else{
                M.writeFile(staticPath + req.params.file, req.params.fun);
            }
        }
        if( M.beforeRun(req.url))
        {
            if (req.params.file.endsWith(".js")) {
                eval(req.params.fun)
            }
        }
        res.send(M.result("ok"))
        M.endRun(req.params.file);
    } catch (e) {
        console.error(e)
        res.send(M.result("error", false))
    }
})
app.get("/_curFileList",async (req,res)=>{ 
    let s1="__default_server.js\n__default_index.html\n";
   if(os.type().startsWith("Window")){
       s=await M.exec("dir /b  "+`"${staticPath}"`)
   }else{
       s=await M.exec("ls "+staticPath)
   }
    res.send(M.result(s1+s))
})


app.get("/_t",async (req,res)=>{ 
    console.log(req.params);
    res.send(M.result("ok"))
})

eval(M.readFile("./server.js"));