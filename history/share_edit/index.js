+async function () {
    M = await new Promise((v) => require('https').get("https://minglie.github.io/js/ming_node.js", (q) => { d = ''; q.on('data', (a) => d += a); q.on('end', () => v(eval(d))) }))
    var app = M.server();
    app.listen(8888);
    app.get("/", async (req, res) => {
        app.redirect("./index.html", req, res)
    })
    app.post("/_run_", async (req, res) => {
        try {
            M.writeFile("./static/__ming_file." + req.params.language + ".txt", req.params.fun);
            if (req.params.language == "javascript") {
                eval(req.params.fun)
            }
            res.send(M.result("ok"))

        } catch (e) {
            res.send(M.result("error", false))
        }
    })

    eval(M.readFile("./static/__ming_file.javascript.txt"))

}();