M.laungeInitValue = {};


M.laungeInitValue["abap"]= `
REPORT zrosetta_base64_encode_data.

DATA: li_client  TYPE REF TO if_http_client,
      lv_encoded TYPE string,
      lv_data    TYPE xstring.


cl_http_client=>create_by_url(
  EXPORTING
    url    = 'http://rosettacode.org/favicon.ico'
  IMPORTING
    client = li_client ).

li_client->send( ).
li_client->receive( ).

lv_data = li_client->response->get_data( ).

CALL FUNCTION 'SSFC_BASE64_ENCODE'
  EXPORTING
    bindata = lv_data
  IMPORTING
    b64data = lv_encoded.

WHILE strlen( lv_encoded ) > 100.
  WRITE: / lv_encoded(100).
  lv_encoded = lv_encoded+100.
ENDWHILE.
WRITE: / lv_encoded.
` 

M.laungeInitValue["apex"]=`
/* Using a single database query, find all the leads in
the database that have the same email address as any
of the leads being inserted or updated. */
for (Lead lead : [SELECT Email FROM Lead WHERE Email IN :leadMap.KeySet()]) {
    Lead newLead = leadMap.get(lead.Email);
    newLead.Email.addError('A lead with this email address already exists.');
}
`

M.laungeInitValue["azcli"]=`
# Create a resource group.
az group create --name myResourceGroup --location westeurope

# Create a new virtual machine, this creates SSH keys if not present.
az vm create --resource-group myResourceGroup --name myVM --image UbuntuLTS --generate-ssh-keys
`

M.laungeInitValue["c"]=`
#include <stdio.h>
 
int main()
{
    /* 我的第一个 C 程序 */
    printf("Hello, World! \n");
 
    return 0;
}
`
M.laungeInitValue["csp"]=`

`

M.laungeInitValue["dockerfile"]=`
FROM centos
RUN yum install wget
RUN wget -O redis.tar.gz "http://download.redis.io/releases/redis-5.0.3.tar.gz"
RUN tar -xvf redis.tar.gz
以上执行会创建 3 层镜像。可简化为以下格式：
FROM centos
RUN yum install wget \
    && wget -O redis.tar.gz "http://download.redis.io/releases/redis-5.0.3.tar.gz" \
    && tar -xvf redis.tar.gz
`

M.laungeInitValue["fsharp"]=`

`

M.laungeInitValue["go"]=`
package main

import "fmt"

func main() {
   fmt.Println("Hello, World!")
}

`
M.laungeInitValue["graphql"]=`

`
M.laungeInitValue["vb"]=`

`



M.laungeInitValue["css"] = `
       #right{
            float:right;
            width:30%;
            height:100%;
        }`;
M.laungeInitValue["html"] = `
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>index</title>
    <script src="https://cdn.staticfile.org/vue/2.4.2/vue.min.js"></script>
</head>

<body>
    <div id="app">
        <h1>网页列表</h1>
        <div v-for="file in list">
            <a :href="file">{{ file }}</a> <br /> <br />
        </div>
    </div>
    <script type="text/javascript">
        new Vue({
            el: '#app',
            data() {
                return {
                    list: null
                }
            },
            mounted() {
                M_this = this;
                fetch('/pagelist').then(function (response) {
                    return response.json();
                }).then(function (response) {
                    let list = response.data.split("\n");
                    list = list.filter((d) => (d.indexOf(".html") >= 0))
                    console.log(list)
                    M_this.list = list
                });
            }

        })
    </script>
</body>
</html>
`;
M.laungeInitValue["java"] = `
package com.example.demo;


public class Application {
	public static void main(String[] args) {
		System.out.println("Hello Word");

	}
}
    `;
M.laungeInitValue["javascript"] = `
         
app.get("/a", (req, res) => {
    console.log(req.params)
    M.add({ s: 77 })
    res.send("https://c-t.work/s/7b10cb04d2754c");
})


app.get("/pagelist",async (req,res)=>{ 
    let s= await M.exec("dir static /b")
    res.send(M.result(s))
})

    `;
M.laungeInitValue["less"] = `
 // LESS
@color: #4D926F;

#header {
  color: @color;
}
h2 {
  color: @color;
}

    `;
M.laungeInitValue["markdown"] = `
**这是加粗的文字**
*这是倾斜的文字*\`
***这是斜体加粗的文字***
~~这是加删除线的文字~~

    `;

M.laungeInitValue["mysql"] = `
select * from resource  limit  (2-1)*5,5;
select count(1) c from resource;
    `;

M.laungeInitValue["php"] = `
<?php

error_reporting(0);

class Result {
	var $code = 3002;
	var $message ="操作成功";
	var $success = true;
	var $data;
}

try {
	$pdo = new PDO("mysql:host=localhost;dbname=ming-lie", "root", "123456");
	$pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e) {
	echo "数据库连接失败：".$e->getMessage();
	exit;
}

$result = new Result;

function doSql(){
	global $pdo;
	global $result;
	try {
		$sql=$_POST["sql"];
		$stmt = $pdo -> prepare($sql);
		$stmt -> execute();
		$row=$stmt->fetchAll(PDO::FETCH_ASSOC);
		$result->data=$row;
		echo (json_encode($result,JSON_UNESCAPED_UNICODE));
	}catch(PDOException $e) {
		echo (json_encode($result,JSON_UNESCAPED_UNICODE));
	}

}
doSql();


    `;

M.laungeInitValue["python"] = `

#!/usr/bin/python
import pymysql
import json
from flask import Flask,request,Response,jsonify
from flask_cors import CORS

# 连接数据库
db  = pymysql.Connect(
    host='localhost',
    port=3306,
    user='root',
    passwd='123456',
    db='test',
    charset='utf8'
)

def dbDoQuerySql(sql):
    cursor = db.cursor()
    cursor.execute(sql)
    result  = cursor.fetchall()
    SqlDomain = cursor.description
    DomainNum = len(SqlDomain)
    SqlDomainName = [None]*DomainNum
    for i in range(DomainNum):
        SqlDomainName[i] = SqlDomain[i][0]
    rresult = []
    for res  in result:
      row={}
      for i in range(len(SqlDomainName)):
        row[SqlDomainName[i]] = res[i]
      rresult.append(row)
    w=[]
    w.append(rresult)
    w.append(SqlDomainName)
    return w

def dbDoInsertUpdateDeleteSql(sql):
    r={};
    cursor = db.cursor()
    try:
      affectedRows=cursor.execute(sql)
      db.commit()
    except:
      db.rollback()
    r["affectedRows"]=affectedRows;
    return  r


def dbDoSql(sql):
    sql=sql.strip()
    if sql.startswith("insert") or sql.startswith("delete") or sql.startswith("update"):
        return  dbDoInsertUpdateDeleteSql(sql)
    else:
        return dbDoQuerySql(sql)


app=Flask(__name__)
CORS(app, supports_credentials=True)

@app.route("/a",methods=["get"])
def a():
    return "ok"



@app.route("/doSql",methods=["post"])
def index1():
    sql=request.form.get("sql");
    print(sql);
    r=dbDoSql(sql)
    res={}
    res["code"]=3002;
    res["message"]="操作成功";
    res["data"]=r
    return jsonify(res)
if __name__ == '__main__':
    app.run(port=8889)

    `;

M.laungeInitValue["redis"] = `
SET runoobkey redis
DEL runoobkey
FLUSHALL
    `;
M.laungeInitValue["typescript"] = `
interface UserArr{
     [index:number]:string
}

var arr:UserArr=['aaa','bbb'];

console.log(arr[0]);

var arr:UserArr=[123,'bbb'];  /*错误*/

console.log(arr[0]);

    `;
M.laungeInitValue["xml"] = `
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.example</groupId>
	<artifactId>demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>demo</name>
	<description>Demo project for Spring Boot</description>
	<build>
	</build>
</project>

    `;
M.laungeInitValue["yaml"] = `
#YAML格式
environments:
    dev:
        url: http://dev.bar.com
        name: Developer Setup
    prod:
        url: http://foo.bar.com
        name: My Cool App
my:
    servers:
        - dev.bar.com
        - foo.bar.com

    `;



M.laungeInitValue["bat"] = `
@echo off

set classpath=.;%cd%/snake1.0_p.jar;
javac -d . *.java

echo ===  running  ===
java cn.itcast.snake.game.PracticeMain
pause
    `;
M.laungeInitValue["cpp"] = `

#include <iostream>
using namespace std;
int main()
{
    cout << "Hello, world!" << endl;
    return 0;
}

    `;
M.laungeInitValue["csharp"] = `
using System;
namespace HelloWorldApplication
{
    class HelloWorld
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.ReadKey();
        }
    }
}
    `;

M.laungeInitValue["shell"] = `
#!/bin/bash
for file in \` ls  *.nc \`
    do
        if [ -d   $file ]
        then
             echo ""
        else
        	echo   $file
             python check.py "$file"
              echo "----------------------------------------"
        fi
done

    `;


M.laungeInitValue["json"] = `
[
    {
        "id": 191,
        "name": "阿里云服务",
        "res_url": "",
        "parent_id": -1,
        "description": "0"
    },
    {
        "id": 192,
        "name": "网站便签",
        "res_url": "",
        "parent_id": -1,
        "description": "null"
    },
    {
        "id": 193,
        "name": "中国知网免费入口",
        "res_url": "http://61.181.120.82:8081/Kns55/",
        "parent_id": 192,
        "description": "null"
    }
]
`




