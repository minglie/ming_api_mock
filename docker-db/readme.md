# 构建
``` sh
docker build -t registry.cn-hangzhou.aliyuncs.com/minglie1/ming_api_mock_db:1.0 .
```
# 执行
``` sh
docker run -it -p:8888:8888  registry.cn-hangzhou.aliyuncs.com/minglie1/ming_api_mock_db:1.0
```
#拉取镜像
``` sh
docker pull registry.cn-hangzhou.aliyuncs.com/minglie1/ming_api_mock_db:1.0
```

# 启动容器
``` sh
docker run -it -p:8888:8888  -v /root/a:/root  --privileged=true  registry.cn-hangzhou.aliyuncs.com/minglie1/ming_api_mock_db:1.0
```
# 如果要用于编辑文件,目标目录应有此文件

server.js
``` js
staticPath="/root/";
console.log("static path="+staticPath)
app.set("views", staticPath);
app.begin((req,res)=>{
    console.log("req ",req.url)
})
M.beforeRun=(r)=>{
    if(r.endsWith("server.js")){
        return true;
    }else{
        return false;
    }  
};

```


# 如果要用于操作mysql

server.js
``` js
Db=M.getMySql({
            "host"     : "127.0.0.1",
            "user"     : "root",
            "password" : "123456",
            "port"     : "3306",
            "database" : "ming",
            multipleStatements: true,
            dateStrings : true ,
            timezone: "08:00"
    });

Db.doSql(`INSERT INTO mi_api VALUES (56, 'wang minglie', '浙江嘉兴', '-1', 'undefined', NULL, '1586185608233', '1586185608233', NULL, '0', NULL, 'POST', '{\"Content-Type\":\"application/json\"}', 'undefined');
INSERT INTO mi_api VALUES (57, 'wang minglie', '浙江嘉兴', '-1', 'undefined', NULL, '1586185611246', '1586185611246', NULL, '1', NULL, 'POST', '{\"Content-Type\":\"application/json\"}', 'undefined');
INSERT INTO mi_api VALUES (58, 'wang minglie', '浙江嘉兴', '56', 'undefined', NULL, '1586185615680', '1586185615680', NULL, '0', NULL, 'POST', '{\"Content-Type\":\"application/json\"}', 'undefined');
INSERT INTO mi_api VALUES (59, 'wang minglie', '浙江嘉兴', '58', 'undefined', NULL, '1586185624337', '1586185624337', NULL, '0', NULL, 'POST', '{\"Content-Type\":\"application/json\"}', 'undefined');
INSERT INTO mi_api VALUES (60, 'wang minglie', '浙江嘉兴', '-1', 'undefined', NULL, '1586185632184', '1586185632184', NULL, '1', NULL, 'POST', '{\"Content-Type\":\"application/json\"}', 'undefined');
INSERT INTO mi_api VALUES (61, 'asf', 'asf', '59', 'undefined', NULL, '1586185645640', '1586185645640', NULL, '1', NULL, 'GET', '{\"Content-Type\":\"application/json\"}', 'undefined');
INSERT INTO mi_api VALUES (62, 'asf', 'asf', '56', 'undefined', NULL, '1586185649664', '1586185649664', NULL, '1', NULL, 'GET', '{\"Content-Type\":\"application/json\"}', 'undefined');`).then(u=>console.log(JSON.stringify(u)))

```
# 如果要用于操作sqlite

server.js
``` js

Db=M.getSqlite()

Db.doSql(`select 1+1`).then(u=>console.log(JSON.stringify(u)))
```

# 扩展

plugins 下为 docker-db的各种扩展
页面通过localStorage.host获取服务地址
