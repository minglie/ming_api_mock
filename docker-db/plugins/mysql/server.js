
myDbconfig={
            "host"     : "127.0.0.1",
            "user"     : "root",
            "password" : "123456",
            "port"     : "3306",
            "database" : "ming"
}


Db=M.getMySql(myDbconfig);



app.get("/getHost",async function (req,res) {
    res.send(M.result("http://localhost:8888") )
})


app.post("/doSql",async function (req,res) {
    try{      
        var rows= await Db.doSql(req.params.sql);
        res.send(M.result(rows));
      
    }catch (e){
        res.send(M.result(e,false));
    }
})


app.get("/getDataBaseName",async function (req,res) {
    res.send(M.result(myDbconfig.database) )
})

app.post("/groupConfig",async function (req,res) {
    M.writeFile("static/js/M_database.js","M_databaseJson="+req.body)
    res.send(M.result("ok") )
})