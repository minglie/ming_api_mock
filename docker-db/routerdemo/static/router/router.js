
/**
 * 默认组件
 */
function dc(props){
  if(props.endsWith(".html")||props.startsWith("http")){
         return ()=><MingWebView src={props}/>
  }
  return <p>{props}</p>
}


const componentMap=(path)=> {
    const map= {
        "/":()=>{window.location.href="/"},
        "/system":()=>dc("/system/resource"),
        "/system/resource":()=>dc("https://www.baidu.com/"),
        "/system/resourceTable":()=>dc("/system/resourceTable"),
        "/system/user":User,
        "/system/role":dc("https://www.baidu.com/"),
        "/system/resourceTree":dc("/system/resourceTree"),
        "/notify":dc("notify"),
        "/notify/template":()=>dc("/notify/resource"),
        "/notify/log":()=>dc("/notify/log1111111")
    }
    return map[path] || dc(path);
}

window.routes = [
    {
      path: "/notify/log",
      name:"百度",
      component: null,
      routes: [
        {
          path: "/notify/log",
          component: dc("https://www.baidu.com/"),
          name:"notifylog"
        },
        {
          path: "/notify/template",
          component: dc("https://www.baidu.com/"),
          name:"notifyTemplate",
        }
      ]
    },
    {
      path: "/system/user",
      name:"系统",
      component:null,
      routes: [
        {
          path: "/system/resource",
          component: dc("https://www.baidu.com/"),
          name:"resource"
        },
        {
          path: "/system/user",
          component: dc("https://www.baidu.com/"),
          name:"User",
        }
      ]
    }
  ];
  

