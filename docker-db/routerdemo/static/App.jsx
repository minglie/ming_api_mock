const { Layout, Menu, Breadcrumb, Icon,Row, Col } = antd;
const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;
const { HashRouter, Route, Link,Switch ,Redirect} = ReactRouterDOM;



eval(M.getAttribute("router"));

/**
 * 通过名字查路由
 * @param {*} routerName 
 */
const getModelRouterByName=(routerName)=>{
    return routes.filter(u=>u.name ==routerName)[0];
}


/**
 * 路由对象转路由组件
 * @param {*} route 
 */
function RouteByRouter(route) {
    return (
      <Route
        exact={route.exact}
        path={route.path}
        render={props => (
          <route.component {...props} routes={route.routes}/>
        )}
      />
    );
  }


  /**
   * 头部导航栏
   */
  class HeaderList extends React.Component {
    render() {
      return (
          <div>
            <div className="logo" />
            <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={['2']}
                style={{ lineHeight: '64px' }}
            >
                    {routes.map((route, i) => {
                            return (
                              <Menu.Item key={route.path}><Link to={route.path}><Icon type={route.icon} theme="twoTone" />{route.name}</Link></Menu.Item>
                            )
                    }
                )}
            </Menu>
          </div>
      );
    }
  }



  
/**
 * 一级路由
 * @param {*} routerName 
 */
  const secondRouter=(routerName)=>{ 
    const MyRouter=getModelRouterByName(routerName);
    class D  extends  React.Component{
      render() {
        return (
            <Switch>
                {MyRouter.routes.map((route, i) => {return (<RouteByRouter key={i} {...route} />)})}
            </Switch>
        );
      }
    }
    return D;
  }
  

  /**
   * 一级路由,依赖二级路由 
   * @param {} routerName 
   */
  const firstRouter=(routerName)=>{ 
    class D extends React.Component {
        componentDidMount() {
    
        }
        handleClick = e => {
            this.props.history.push(e.key)
        };
        render() {
            let SystemRouter = secondRouter(routerName);
            return (
                <div>
                    <Layout>
                        <Sider collapsible width={200} style={{ background: '#fff' }}>
                            <Menu>
                                {getModelRouterByName(routerName)["routes"].map(
                                    route => {
                                        return (
                                            <Menu.Item key={route.path}><Link to={route.path}><Icon type={route.icon} theme="twoTone" />{route.name}</Link></Menu.Item>
                                        )
                                    })}
    
                            </Menu>
                        </Sider>
                        <Layout style={{ padding: '0 24px 24px' }}>
                            <Content
                                style={{
                                    background: '#fff',
                                    padding: 24,
                                    margin: 0,
                                    minHeight: 2000,
                                }}
                            >
                                <SystemRouter />
                            </Content>
                        </Layout>
                    </Layout>
                </div>
            );
        }
    }
    return D;
  }

 
class App extends React.Component {
    state = {
        collapsed: false,
        
    };
    onCollapse = (collapsed) => {
        this.setState({ collapsed });
    }
    render() {
        return (
            <HashRouter>
                <Layout>
                    <Header className="header">
                       <HeaderList/>
                    </Header>
                    <Layout>
                         {routes.map((route, i) => { return ( <Route key={i} path={"/"+route.path.split("/")[1]} component={ firstRouter(route.name)} />)})}  
                    </Layout>
                </Layout>
            </HashRouter>
        );
    }
}