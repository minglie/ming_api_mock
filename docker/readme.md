# 构建
``` sh
docker build -t registry.cn-hangzhou.aliyuncs.com/minglie1/ming_api_mock:1.0 .
```
# 执行
``` sh
docker run -it -p:8888:8888  registry.cn-hangzhou.aliyuncs.com/minglie1/ming_api_mock:1.0
```
#拉取镜像
``` sh
docker pull registry.cn-hangzhou.aliyuncs.com/minglie1/ming_api_mock:1.0
```

# 启动容器
``` sh
docker run -it -p:8888:8888  -v /root/a:/root  --privileged=true  registry.cn-hangzhou.aliyuncs.com/minglie1/ming_api_mock:1.0
```
# 如果要用于编辑文件,目标目录应有此文件

server.js
``` js
staticPath="/root/";
console.log("static path="+staticPath)
app.set("views", staticPath);
app.begin((req,res)=>{
    console.log("req ",req.url)
})
M.beforeRun=(r)=>{
    if(r.endsWith("server.js")){
        return true;
    }else{
        return false;
    }  
};

```
